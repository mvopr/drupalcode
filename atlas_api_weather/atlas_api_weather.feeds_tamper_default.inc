<?php

/**
 * Implements hook_feeds_tamper_default().
 */
function atlas_api_weather_feeds_tamper_default() {
  $feeds_tampers = array();

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'us_weather-location-remove_and';
  $feeds_tamper->importer = 'us_weather';
  $feeds_tamper->source = 'location';
  $feeds_tamper->plugin_id = 'find_replace_regex';
  $feeds_tamper->settings = array(
    'find' => '/(and).*$/',
    'replace' => '',
    'limit' => '',
    'real_limit' => -1,
  );
  $feeds_tamper->weight = 1;
  $feeds_tamper->description = 'remove_and';
  $feeds_tampers['us_weather-location-remove_and'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE;
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'us_weather-location-remove_semi';
  $feeds_tamper->importer = 'us_weather';
  $feeds_tamper->source = 'location';
  $feeds_tamper->plugin_id = 'find_replace_regex';
  $feeds_tamper->settings = array(
    'find' => '/;.*$/',
    'replace' => '',
    'limit' => '',
    'real_limit' => -1,
  );
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = 'remove_semi';
  $feeds_tampers['us_weather-location-remove_semi'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'us_weather-location-remove_to';
  $feeds_tamper->importer = 'us_weather';
  $feeds_tamper->source = 'location';
  $feeds_tamper->plugin_id = 'find_replace_regex';
  $feeds_tamper->settings = array(
    'find' => '/(to).*$/',
    'replace' => '',
    'limit' => '',
    'real_limit' => -1,
  );
  $feeds_tamper->weight = 2;
  $feeds_tamper->description = 'remove_to';
  $feeds_tampers['us_weather-location-remove_to'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'us_weather-location-rewrite';
  $feeds_tamper->importer = 'us_weather';
  $feeds_tamper->source = 'location';
  $feeds_tamper->plugin_id = 'rewrite';
  $feeds_tamper->settings = array(
    'text' => '[location], [temps] USA',
  );
  $feeds_tamper->weight = 3;
  $feeds_tamper->description = 'Rewrite';
  $feeds_tampers['us_weather-location-rewrite'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'us_weather-temps-before_url';
  $feeds_tamper->importer = 'us_weather';
  $feeds_tamper->source = 'temps';
  $feeds_tamper->plugin_id = 'find_replace_regex';
  $feeds_tamper->settings = array(
    'find' => '/.*=/i',
    'replace' => '',
    'limit' => '',
    'real_limit' => -1,
  );
  $feeds_tamper->weight = 2;
  $feeds_tamper->description = 'Remove Before';
  $feeds_tampers['us_weather-temps-before_url'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'us_weather-temps-remove_after';
  $feeds_tamper->importer = 'us_weather';
  $feeds_tamper->source = 'temps';
  $feeds_tamper->plugin_id = 'find_replace_regex';
  $feeds_tamper->settings = array(
    'find' => '/(?<=^..)(.*)/',
    'replace' => '',
    'limit' => '',
    'real_limit' => -1,
  );
  $feeds_tamper->weight = 3;
  $feeds_tamper->description = 'Remove After Char';
  $feeds_tampers['us_weather-temps-remove_after'] = $feeds_tamper;

  return $feeds_tampers;
}
