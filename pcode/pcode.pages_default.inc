<?php

/**
 * Implements hook_default_page_manager_pages().
 * @return array a list of pages
 */
function pcode_default_page_manager_pages() {
  // Get exported  files
  $module_path = drupal_get_path('module', 'pcode');
  $files = file_scan_directory($module_path . '/pages', '/.inc/');
 
  $pages = array();
 
  foreach ($files as $file_path => $file) {
    require $file_path;
 
    if (isset($page)) {
      $pages[$page->name] = $page;
    }
  }
 
  return $pages;
}

function pcode_default_page_manager_handlers() {
  // Get exported  files
  $module_path = drupal_get_path('module', 'pcode');
  $files = file_scan_directory($module_path . '/handlers', '/.inc/');
 
  $handlers = array();
 
  foreach ($files as $file_path => $file) {
    require $file_path;
 
    if (isset($handler)) {
      $handlers[$handler->name] = $handler;
    }
  }
 
  return $handlers;
}