<?php

$page = new stdClass();
$page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
$page->api_version = 1;
$page->name = 'pan_discussions';
$page->task = 'page';
$page->admin_title = 'Discussions';
$page->admin_description = '';
$page->path = 'discussions';
$page->access = array();
$page->menu = array();
$page->arguments = array();
$page->conf = array(
  'admin_paths' => FALSE,
);
$page->default_handlers = array();
$handler = new stdClass();
$handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
$handler->api_version = 1;
$handler->name = 'page_discussions_panel_context_2';
$handler->task = 'page';
$handler->subtask = 'pan_discussions';
$handler->handler = 'panel_context';
$handler->weight = 1;
$handler->conf = array(
  'title' => 'Panel',
  'no_blocks' => 0,
  'pipeline' => 'standard',
  'body_classes_to_remove' => '',
  'body_classes_to_add' => 'blur',
  'css_id' => '',
  'css' => '',
  'contexts' => array(),
  'relationships' => array(),
  'access' => array(
    'plugins' => array(
      0 => array(
        'name' => 'role',
        'settings' => array(
          'rids' => array(
            0 => 84425795,
          ),
        ),
        'context' => 'logged-in-user',
        'not' => FALSE,
      ),
    ),
    'logic' => 'and',
  ),
);
$display = new panels_display();
$display->layout = 'onecol';
$display->layout_settings = array();
$display->panel_settings = array(
  'style_settings' => array(
    'default' => NULL,
    'middle' => NULL,
  ),
);
$display->cache = array();
$display->title = 'Discussions';
$display->uuid = '7477c320-6409-4102-b3dd-3ff5a3524aa6';
$display->content = array();
$display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-bee10694-95aa-47c2-a32e-d29e0e09d8ef';
  $pane->panel = 'middle';
  $pane->type = 'views';
  $pane->subtype = 'taxonomy_view';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_pager_settings' => 0,
    'use_pager' => 0,
    'nodes_per_page' => '0',
    'pager_id' => '0',
    'offset' => '0',
    'more_link' => 0,
    'feed_icons' => 0,
    'panel_args' => 0,
    'link_to_view' => 0,
    'args' => '',
    'url' => '',
    'display' => 'block',
    'override_title' => 1,
    'override_title_text' => '',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array(
    'css_id' => 'discussion-sidebar',
    'css_class' => 'second-nav',
  );
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = 'bee10694-95aa-47c2-a32e-d29e0e09d8ef';
  $display->content['new-bee10694-95aa-47c2-a32e-d29e0e09d8ef'] = $pane;
  $display->panels['middle'][0] = 'new-bee10694-95aa-47c2-a32e-d29e0e09d8ef';
  $pane = new stdClass();
  $pane->pid = 'new-66ad8ada-526a-40b2-a8c1-aecec4811a31';
  $pane->panel = 'middle';
  $pane->type = 'views';
  $pane->subtype = 'discussions';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_pager_settings' => 0,
    'use_pager' => 1,
    'nodes_per_page' => '10',
    'pager_id' => '0',
    'offset' => '0',
    'more_link' => 0,
    'feed_icons' => 0,
    'panel_args' => 0,
    'link_to_view' => 0,
    'args' => '',
    'url' => '',
    'display' => 'block_1',
    'override_title' => 1,
    'override_title_text' => '',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array(
    'css_id' => '',
    'css_class' => 'two-col',
  );
  $pane->extras = array();
  $pane->position = 1;
  $pane->locks = array();
  $pane->uuid = '66ad8ada-526a-40b2-a8c1-aecec4811a31';
  $display->content['new-66ad8ada-526a-40b2-a8c1-aecec4811a31'] = $pane;
  $display->panels['middle'][1] = 'new-66ad8ada-526a-40b2-a8c1-aecec4811a31';
$display->hide_title = PANELS_TITLE_FIXED;
$display->title_pane = '0';
$handler->conf['display'] = $display;
$page->default_handlers[$handler->name] = $handler;
