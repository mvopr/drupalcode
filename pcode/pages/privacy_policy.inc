<?php

$page = new stdClass();
$page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
$page->api_version = 1;
$page->name = 'privacy_policy';
$page->task = 'page';
$page->admin_title = 'Privacy Policy';
$page->admin_description = '';
$page->path = 'privacy-policy';
$page->access = array();
$page->menu = array();
$page->arguments = array();
$page->conf = array(
  'admin_paths' => FALSE,
);
$page->default_handlers = array();
$handler = new stdClass();
$handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
$handler->api_version = 1;
$handler->name = 'page_privacy_policy_panel_context';
$handler->task = 'page';
$handler->subtask = 'privacy_policy';
$handler->handler = 'panel_context';
$handler->weight = 0;
$handler->conf = array(
  'title' => 'Panel',
  'no_blocks' => 0,
  'pipeline' => 'standard',
  'body_classes_to_remove' => '',
  'body_classes_to_add' => 'external',
  'css_id' => '',
  'css' => '',
  'contexts' => array(),
  'relationships' => array(),
  'access' => array(
    'plugins' => array(),
    'logic' => 'and',
  ),
);
$display = new panels_display();
$display->layout = 'flexible';
$display->layout_settings = array();
$display->panel_settings = array(
  'style_settings' => array(
    'default' => NULL,
    'center' => NULL,
  ),
);
$display->cache = array();
$display->title = 'Privacy Policy';
$display->uuid = '255b2dcc-a597-444b-80d6-a6454fcb63ff';
$display->content = array();
$display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-7f49bd45-cc56-4800-8166-049b0eb3a8e4';
  $pane->panel = 'center';
  $pane->type = 'custom';
  $pane->subtype = 'custom';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'admin_title' => 'Privacy Block',
    'title' => '',
    'body' => '<script>
(function ($) {
$( document ).ready(function() {
    $( "#load-text" ).load( "../index/privacy.php #legal-pan" );
});
})(jQuery);

</script>
<div id="load-text"></div>',
    'format' => 'full_html',
    'substitute' => TRUE,
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array(
    'css_id' => '',
    'css_class' => 'body-style',
  );
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '7f49bd45-cc56-4800-8166-049b0eb3a8e4';
  $display->content['new-7f49bd45-cc56-4800-8166-049b0eb3a8e4'] = $pane;
  $display->panels['center'][0] = 'new-7f49bd45-cc56-4800-8166-049b0eb3a8e4';
  $pane = new stdClass();
  $pane->pid = 'new-c2d341a5-f4e8-4f1b-b71e-461ca153b56a';
  $pane->panel = 'center';
  $pane->type = 'custom';
  $pane->subtype = 'custom';
  $pane->shown = TRUE;
  $pane->access = array(
    'plugins' => array(
      0 => array(
        'name' => 'theme',
        'settings' => array(
          'theme' => 'thebulletin',
        ),
        'not' => FALSE,
      ),
    ),
  );
  $pane->configuration = array(
    'admin_title' => 'Mobile Back',
    'title' => '',
    'body' => '<style>
    #back-btn2 {
    	display: block;
    	width: 35px;
    	float: left;
    	height: 35px;
    	background: url(\'http://mv.local/oprsndo/themecore/back-btn.png\') center 7px / 20px 20px no-repeat;
    	margin-top: 25px;
    }
    
    #back-btn {
    	display: none;
    }
    </style>',
    'format' => 'full_html',
    'substitute' => TRUE,
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 1;
  $pane->locks = array();
  $pane->uuid = 'c2d341a5-f4e8-4f1b-b71e-461ca153b56a';
  $display->content['new-c2d341a5-f4e8-4f1b-b71e-461ca153b56a'] = $pane;
  $display->panels['center'][1] = 'new-c2d341a5-f4e8-4f1b-b71e-461ca153b56a';
$display->hide_title = PANELS_TITLE_FIXED;
$display->title_pane = 'new-7f49bd45-cc56-4800-8166-049b0eb3a8e4';
$handler->conf['display'] = $display;
$page->default_handlers[$handler->name] = $handler;
