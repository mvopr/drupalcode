<?php

$page = new stdClass();
$page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
$page->api_version = 1;
$page->name = 'upgrade';
$page->task = 'page';
$page->admin_title = 'Upgrade';
$page->admin_description = 'Upgrade user account page.';
$page->path = 'upgrade';
$page->access = array(
  'plugins' => array(
    0 => array(
      'name' => 'role',
      'settings' => array(
        'rids' => array(
          0 => 84425795,
        ),
      ),
      'context' => 'logged-in-user',
      'not' => FALSE,
    ),
    1 => array(
      'name' => 'role',
      'settings' => array(
        'rids' => array(
          0 => 215641394,
        ),
      ),
      'context' => 'logged-in-user',
      'not' => TRUE,
    ),
  ),
  'logic' => 'and',
);
$page->menu = array();
$page->arguments = array();
$page->conf = array(
  'admin_paths' => FALSE,
);
$page->default_handlers = array();
$handler = new stdClass();
$handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
$handler->api_version = 1;
$handler->name = 'page_upgrade__panel_context_29e89311-03d9-4e91-bdef-778c142214c4';
$handler->task = 'page';
$handler->subtask = 'upgrade';
$handler->handler = 'panel_context';
$handler->weight = 0;
$handler->conf = array(
  'title' => 'Panel',
  'no_blocks' => 0,
  'pipeline' => 'standard',
  'body_classes_to_remove' => '',
  'body_classes_to_add' => '',
  'css_id' => '',
  'css' => '',
  'contexts' => array(),
  'relationships' => array(),
);
$display = new panels_display();
$display->layout = 'onecol';
$display->layout_settings = array();
$display->panel_settings = array(
  'style_settings' => array(
    'default' => NULL,
    'middle' => NULL,
  ),
);
$display->cache = array();
$display->title = '';
$display->uuid = '810e3773-b5d1-479e-ba83-cd25d27ffd1b';
$display->content = array();
$display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-b8ec2b80-1aba-4f0d-b3f0-bd3071639921';
  $pane->panel = 'middle';
  $pane->type = 'custom';
  $pane->subtype = 'custom';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'admin_title' => 'Nav',
    'title' => '',
    'body' => '<ul class="member-mng-nav">
<li><a href="?q=user">General</a></li>
<li><a href="?q=upgrade">Subscription</a></li>
</ul>',
    'format' => 'full_html',
    'substitute' => TRUE,
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = 'b8ec2b80-1aba-4f0d-b3f0-bd3071639921';
  $display->content['new-b8ec2b80-1aba-4f0d-b3f0-bd3071639921'] = $pane;
  $display->panels['middle'][0] = 'new-b8ec2b80-1aba-4f0d-b3f0-bd3071639921';
  $pane = new stdClass();
  $pane->pid = 'new-82d52996-8862-4a48-9df0-b9b5d8f0e10b';
  $pane->panel = 'middle';
  $pane->type = 'custom';
  $pane->subtype = 'custom';
  $pane->shown = TRUE;
  $pane->access = array(
    'plugins' => array(
      0 => array(
        'name' => 'role',
        'settings' => array(
          'rids' => array(
            0 => 225332102,
            1 => 172216448,
            2 => 185875538,
          ),
        ),
        'context' => 'logged-in-user',
        'not' => FALSE,
      ),
    ),
  );
  $pane->configuration = array(
    'admin_title' => 'Thank You',
    'title' => '',
    'body' => '<h1>Thank you for being our customer!</h1>
',
    'format' => 'full_html',
    'substitute' => TRUE,
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array(
    'css_id' => '',
    'css_class' => 'upgrade-tag thankyou',
  );
  $pane->extras = array();
  $pane->position = 1;
  $pane->locks = array();
  $pane->uuid = '82d52996-8862-4a48-9df0-b9b5d8f0e10b';
  $display->content['new-82d52996-8862-4a48-9df0-b9b5d8f0e10b'] = $pane;
  $display->panels['middle'][1] = 'new-82d52996-8862-4a48-9df0-b9b5d8f0e10b';
  $pane = new stdClass();
  $pane->pid = 'new-a9c46d2a-df65-4036-a343-2f084659749f';
  $pane->panel = 'middle';
  $pane->type = 'custom';
  $pane->subtype = 'custom';
  $pane->shown = TRUE;
  $pane->access = array(
    'plugins' => array(
      0 => array(
        'name' => 'role',
        'settings' => array(
          'rids' => array(
            0 => 225332102,
            1 => 172216448,
            2 => 185875538,
          ),
        ),
        'context' => 'logged-in-user',
        'not' => TRUE,
      ),
    ),
  );
  $pane->configuration = array(
    'admin_title' => 'Tagline',
    'title' => '',
    'body' => '
    
    <div class="progress">
    	<div class="progress-item active"><span class="dotted-ciricle">1</span><br>Select</div>
    	<div class="progress-item"><span class="dotted-ciricle">2</span><br>Pay</div>
    	<div class="progress-item"><span class="dotted-ciricle">3</span><br>Done!</div>
    </div>
    <h1>We can\'t wait for you to join us!</h1>
    <h2>First, select a pay-as-you-go, no contract monthly plan.</h2>
    
    ',
    'format' => 'full_html',
    'substitute' => TRUE,
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array(
    'css_id' => '',
    'css_class' => 'upgrade-tag',
  );
  $pane->extras = array();
  $pane->position = 2;
  $pane->locks = array();
  $pane->uuid = 'a9c46d2a-df65-4036-a343-2f084659749f';
  $display->content['new-a9c46d2a-df65-4036-a343-2f084659749f'] = $pane;
  $display->panels['middle'][2] = 'new-a9c46d2a-df65-4036-a343-2f084659749f';
  $pane = new stdClass();
  $pane->pid = 'new-75b61a0b-acd7-4d0e-bb70-508c04bcc00d';
  $pane->panel = 'middle';
  $pane->type = 'panels_mini';
  $pane->subtype = 'reporter_upgrade_box';
  $pane->shown = TRUE;
  $pane->access = array(
    'plugins' => array(
      0 => array(
        'name' => 'role',
        'settings' => array(
          'rids' => array(
            0 => 225332102,
            1 => 172216448,
            2 => 185875538,
          ),
        ),
        'context' => 'logged-in-user',
        'not' => TRUE,
      ),
    ),
  );
  $pane->configuration = array(
    'override_title' => 0,
    'override_title_text' => '',
    'override_title_heading' => 'h2',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array(
    'css_id' => '',
    'css_class' => 'no-role',
  );
  $pane->extras = array();
  $pane->position = 3;
  $pane->locks = array();
  $pane->uuid = '75b61a0b-acd7-4d0e-bb70-508c04bcc00d';
  $display->content['new-75b61a0b-acd7-4d0e-bb70-508c04bcc00d'] = $pane;
  $display->panels['middle'][3] = 'new-75b61a0b-acd7-4d0e-bb70-508c04bcc00d';
  $pane = new stdClass();
  $pane->pid = 'new-26020268-3e59-4123-b4e0-fc9151b0f59d';
  $pane->panel = 'middle';
  $pane->type = 'panels_mini';
  $pane->subtype = 'reporter_upgrade_box';
  $pane->shown = TRUE;
  $pane->access = array(
    'plugins' => array(
      0 => array(
        'name' => 'role',
        'settings' => array(
          'rids' => array(
            0 => 172216448,
          ),
        ),
        'context' => 'logged-in-user',
        'not' => FALSE,
      ),
    ),
  );
  $pane->configuration = array(
    'override_title' => 0,
    'override_title_text' => '',
    'override_title_heading' => 'h2',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array(
    'css_id' => '',
    'css_class' => 'freelance-active',
  );
  $pane->extras = array();
  $pane->position = 4;
  $pane->locks = array();
  $pane->uuid = '26020268-3e59-4123-b4e0-fc9151b0f59d';
  $display->content['new-26020268-3e59-4123-b4e0-fc9151b0f59d'] = $pane;
  $display->panels['middle'][4] = 'new-26020268-3e59-4123-b4e0-fc9151b0f59d';
  $pane = new stdClass();
  $pane->pid = 'new-dfed1576-6b2e-4b79-8438-f88bdf7f3b82';
  $pane->panel = 'middle';
  $pane->type = 'panels_mini';
  $pane->subtype = 'reporter_upgrade_box';
  $pane->shown = TRUE;
  $pane->access = array(
    'plugins' => array(
      0 => array(
        'name' => 'role',
        'settings' => array(
          'rids' => array(
            0 => 225332102,
          ),
        ),
        'context' => 'logged-in-user',
        'not' => FALSE,
      ),
    ),
  );
  $pane->configuration = array(
    'override_title' => 0,
    'override_title_text' => '',
    'override_title_heading' => 'h2',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array(
    'css_id' => '',
    'css_class' => 'basic-active ',
  );
  $pane->extras = array();
  $pane->position = 5;
  $pane->locks = array();
  $pane->uuid = 'dfed1576-6b2e-4b79-8438-f88bdf7f3b82';
  $display->content['new-dfed1576-6b2e-4b79-8438-f88bdf7f3b82'] = $pane;
  $display->panels['middle'][5] = 'new-dfed1576-6b2e-4b79-8438-f88bdf7f3b82';
  $pane = new stdClass();
  $pane->pid = 'new-84649d7e-c563-49c8-98ba-bdcd1193f4e9';
  $pane->panel = 'middle';
  $pane->type = 'panels_mini';
  $pane->subtype = 'reporter_upgrade_box';
  $pane->shown = TRUE;
  $pane->access = array(
    'plugins' => array(
      0 => array(
        'name' => 'role',
        'settings' => array(
          'rids' => array(
            0 => 185875538,
          ),
        ),
        'context' => 'logged-in-user',
        'not' => FALSE,
      ),
    ),
  );
  $pane->configuration = array(
    'override_title' => 0,
    'override_title_text' => '',
    'override_title_heading' => 'h2',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array(
    'css_id' => '',
    'css_class' => 'pro-active',
  );
  $pane->extras = array();
  $pane->position = 6;
  $pane->locks = array();
  $pane->uuid = '84649d7e-c563-49c8-98ba-bdcd1193f4e9';
  $display->content['new-84649d7e-c563-49c8-98ba-bdcd1193f4e9'] = $pane;
  $display->panels['middle'][6] = 'new-84649d7e-c563-49c8-98ba-bdcd1193f4e9';
  $pane = new stdClass();
  $pane->pid = 'new-74fb6ab4-a230-4da2-8791-605593d62d21';
  $pane->panel = 'middle';
  $pane->type = 'custom';
  $pane->subtype = 'custom';
  $pane->shown = TRUE;
  $pane->access = array(
    'plugins' => array(
      0 => array(
        'name' => 'role',
        'settings' => array(
          'rids' => array(
            0 => 225332102,
            1 => 172216448,
            2 => 185875538,
          ),
        ),
        'context' => 'logged-in-user',
        'not' => TRUE,
      ),
    ),
  );
  $pane->configuration = array(
    'admin_title' => 'Sales Bar',
    'title' => '',
    'body' => '<div class="sales-bar">
   <div class="left-box">
      <div id="mail-letter"></div>
      <div>Not sure which plan is right for you?<br><a href="mailto:support@openreporter.org?Subject=Upgrade%20Question">Let us help</a></div>
   </div>
   <div class="right-box">
       <h3>All plans include:</h3>
       <ul>
           <li>Friendly customer service</li>
           <li>Full access to Bulletins and Beacons</li>
           <li>Unlimited notes</li>
       </ul>
   </div>
   
   <div id="disclamer">
   <ul>
   <li><span class="highlight">Cancellations and Refunds:</span> If you cancel a plan, the cancellation may take  1-3 business days to competently process. We do not offer or honor pro-rated refunds on any plan.</li>
   <li><span class="highlight">Upgrades:</span> All plan upgrades will be deemed as creating a new separate subscription. At the start of a new subscription the customer will not be eligible for former pricing.</li>
   </ul>
   </div>

   <div class="bot-box">Is there a feature you think we should include?<br><a href="mailto:support@openreporter.org?Subject=Feedback">We want to know!</a></div> 
   
     
</div>',
    'format' => 'full_html',
    'substitute' => TRUE,
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 7;
  $pane->locks = array();
  $pane->uuid = '74fb6ab4-a230-4da2-8791-605593d62d21';
  $display->content['new-74fb6ab4-a230-4da2-8791-605593d62d21'] = $pane;
  $display->panels['middle'][7] = 'new-74fb6ab4-a230-4da2-8791-605593d62d21';
  $pane = new stdClass();
  $pane->pid = 'new-0809993d-d55c-4192-ae6f-fb927aeabb02';
  $pane->panel = 'middle';
  $pane->type = 'custom';
  $pane->subtype = 'custom';
  $pane->shown = TRUE;
  $pane->access = array(
    'plugins' => array(
      0 => array(
        'name' => 'role',
        'settings' => array(
          'rids' => array(
            0 => 225332102,
            1 => 172216448,
            2 => 185875538,
          ),
        ),
        'context' => 'logged-in-user',
        'not' => FALSE,
      ),
    ),
  );
  $pane->configuration = array(
    'admin_title' => 'Upgraded Notics',
    'title' => '',
    'body' => '<div class="sales-bar">
   <div class="left-box">
      <div id="mail-letter"></div>
      <div>Have questions about your plan?<br><a href="mailto:support@openreporter.org?Subject=Plan%20Question">Contact Us</a></div>
   </div>
   <div class="right-box" id="hand-text" >
       <span class="highlight">Tip:</span> To change your plan first unsubscribe from your current plan and then select a new one.
   </div>
   
   <div id="disclamer">
   <ul>
   <li><span class="highlight">Cancellations and Refunds:</span> If you cancel a plan, the cancellation may take  1-3 business days to competently process. We do not offer or honor pro-rated refunds on any plan.</li>
   <li><span class="highlight">Upgrades:</span> All plan upgrades will be deemed as creating a new separate subscription. At the start of a new subscription the customer will not be eligible for former pricing.</li>
   </ul>
   </div>
   
   <div class="bot-box">Is there a feature you think we should include?<br><a href="mailto:support@openreporter.org?Subject=Feedback">We want to know!</a></div>
</div>',
    'format' => 'full_html',
    'substitute' => TRUE,
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 8;
  $pane->locks = array();
  $pane->uuid = '0809993d-d55c-4192-ae6f-fb927aeabb02';
  $display->content['new-0809993d-d55c-4192-ae6f-fb927aeabb02'] = $pane;
  $display->panels['middle'][8] = 'new-0809993d-d55c-4192-ae6f-fb927aeabb02';
$display->hide_title = PANELS_TITLE_FIXED;
$display->title_pane = '0';
$handler->conf['display'] = $display;
$page->default_handlers[$handler->name] = $handler;
