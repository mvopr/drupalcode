<?php

$page = new stdClass();
$page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
$page->api_version = 1;
$page->name = 'notes_main';
$page->task = 'page';
$page->admin_title = 'Notes';
$page->admin_description = '';
$page->path = 'notes';
$page->access = array(
  'plugins' => array(
    0 => array(
      'name' => 'role',
      'settings' => array(
        'rids' => array(
          0 => 2,
        ),
      ),
      'context' => 'logged-in-user',
      'not' => FALSE,
    ),
  ),
  'logic' => 'and',
);
$page->menu = array();
$page->arguments = array();
$page->conf = array(
  'admin_paths' => FALSE,
);
$page->default_handlers = array();
$handler = new stdClass();
$handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
$handler->api_version = 1;
$handler->name = 'page_notes_panel_context';
$handler->task = 'page';
$handler->subtask = 'notes_main';
$handler->handler = 'panel_context';
$handler->weight = 0;
$handler->conf = array(
  'title' => 'Note Main',
  'no_blocks' => 0,
  'pipeline' => 'standard',
  'body_classes_to_remove' => '',
  'body_classes_to_add' => '',
  'css_id' => '',
  'css' => '',
  'contexts' => array(),
  'relationships' => array(),
  'access' => array(
    'plugins' => array(
      0 => array(
        'name' => 'role',
        'settings' => array(
          'rids' => array(
            0 => 225332102,
            1 => 172216448,
            2 => 185875538,
            3 => 40207975,
            4 => 203373462,
          ),
        ),
        'context' => 'logged-in-user',
        'not' => FALSE,
      ),
    ),
    'logic' => 'and',
  ),
);
$display = new panels_display();
$display->layout = 'onecol';
$display->layout_settings = array();
$display->panel_settings = array(
  'style_settings' => array(
    'default' => NULL,
    'middle' => NULL,
  ),
);
$display->cache = array();
$display->title = '';
$display->uuid = '0c9a5df8-4625-43b6-85df-529ccbb32aa1';
$display->content = array();
$display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-a469409d-0cbf-4873-b1d9-1efd0d2c488c';
  $pane->panel = 'middle';
  $pane->type = 'block';
  $pane->subtype = 'block-5';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => 1,
    'override_title_text' => '',
    'override_title_heading' => 'h2',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = 'a469409d-0cbf-4873-b1d9-1efd0d2c488c';
  $display->content['new-a469409d-0cbf-4873-b1d9-1efd0d2c488c'] = $pane;
  $display->panels['middle'][0] = 'new-a469409d-0cbf-4873-b1d9-1efd0d2c488c';
  $pane = new stdClass();
  $pane->pid = 'new-aee6c281-0ecb-44d9-a451-f8936bf64970';
  $pane->panel = 'middle';
  $pane->type = 'views';
  $pane->subtype = 'notes';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_pager_settings' => 0,
    'use_pager' => 1,
    'nodes_per_page' => '10',
    'pager_id' => '0',
    'offset' => '0',
    'more_link' => 0,
    'feed_icons' => 0,
    'panel_args' => 0,
    'link_to_view' => 0,
    'args' => '',
    'url' => '',
    'display' => 'block_1',
    'override_title' => 1,
    'override_title_text' => '',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array(
    'css_id' => 'notes-sidebar',
    'css_class' => 'second-nav',
  );
  $pane->extras = array();
  $pane->position = 1;
  $pane->locks = array();
  $pane->uuid = 'aee6c281-0ecb-44d9-a451-f8936bf64970';
  $display->content['new-aee6c281-0ecb-44d9-a451-f8936bf64970'] = $pane;
  $display->panels['middle'][1] = 'new-aee6c281-0ecb-44d9-a451-f8936bf64970';
  $pane = new stdClass();
  $pane->pid = 'new-3a23cb25-7882-4bfd-83d4-a37f8d772d15';
  $pane->panel = 'middle';
  $pane->type = 'block';
  $pane->subtype = 'opr_embed_form-opr_embed_form_note_node_form';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => 1,
    'override_title_text' => '',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array(
    'css_id' => '',
    'css_class' => 'single-col-body',
  );
  $pane->extras = array();
  $pane->position = 2;
  $pane->locks = array();
  $pane->uuid = '3a23cb25-7882-4bfd-83d4-a37f8d772d15';
  $display->content['new-3a23cb25-7882-4bfd-83d4-a37f8d772d15'] = $pane;
  $display->panels['middle'][2] = 'new-3a23cb25-7882-4bfd-83d4-a37f8d772d15';
$display->hide_title = PANELS_TITLE_FIXED;
$display->title_pane = '0';
$handler->conf['display'] = $display;
$page->default_handlers[$handler->name] = $handler;
$handler = new stdClass();
$handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
$handler->api_version = 1;
$handler->name = 'page_notes_main__r0_view';
$handler->task = 'page';
$handler->subtask = 'notes_main';
$handler->handler = 'panel_context';
$handler->weight = 1;
$handler->conf = array(
  'title' => 'R0 View',
  'no_blocks' => 0,
  'pipeline' => 'standard',
  'body_classes_to_remove' => '',
  'body_classes_to_add' => '',
  'css_id' => '',
  'css' => '',
  'contexts' => array(),
  'relationships' => array(),
  'name' => 'r0_view',
);
$display = new panels_display();
$display->layout = 'onecol';
$display->layout_settings = array();
$display->panel_settings = array(
  'style_settings' => array(
    'default' => NULL,
    'middle' => NULL,
  ),
);
$display->cache = array();
$display->title = '';
$display->uuid = 'b12fd3d3-e8a7-4f91-9f0a-265eb4b5547f';
$display->content = array();
$display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-23b2e0ec-f1e8-4041-a759-3ec3fc8ffe79';
  $pane->panel = 'middle';
  $pane->type = 'panels_mini';
  $pane->subtype = 'micro_note';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => 1,
    'override_title_text' => '',
    'override_title_heading' => 'h2',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array(
    'css_id' => '',
    'css_class' => 'no-notes',
  );
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '23b2e0ec-f1e8-4041-a759-3ec3fc8ffe79';
  $display->content['new-23b2e0ec-f1e8-4041-a759-3ec3fc8ffe79'] = $pane;
  $display->panels['middle'][0] = 'new-23b2e0ec-f1e8-4041-a759-3ec3fc8ffe79';
$display->hide_title = PANELS_TITLE_FIXED;
$display->title_pane = 'new-23b2e0ec-f1e8-4041-a759-3ec3fc8ffe79';
$handler->conf['display'] = $display;
$page->default_handlers[$handler->name] = $handler;
