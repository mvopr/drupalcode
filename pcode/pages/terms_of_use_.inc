<?php

$page = new stdClass();
$page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
$page->api_version = 1;
$page->name = 'terms_of_use_';
$page->task = 'page';
$page->admin_title = 'Terms of Use';
$page->admin_description = '';
$page->path = 'terms-use';
$page->access = array();
$page->menu = array();
$page->arguments = array();
$page->conf = array(
  'admin_paths' => FALSE,
);
$page->default_handlers = array();
$handler = new stdClass();
$handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
$handler->api_version = 1;
$handler->name = 'page_terms_of_use__panel_context';
$handler->task = 'page';
$handler->subtask = 'terms_of_use_';
$handler->handler = 'panel_context';
$handler->weight = 0;
$handler->conf = array(
  'title' => 'Panel',
  'no_blocks' => 0,
  'pipeline' => 'standard',
  'body_classes_to_remove' => '',
  'body_classes_to_add' => 'external',
  'css_id' => '',
  'css' => '',
  'contexts' => array(),
  'relationships' => array(),
  'access' => array(
    'plugins' => array(),
    'logic' => 'and',
  ),
);
$display = new panels_display();
$display->layout = 'flexible';
$display->layout_settings = array();
$display->panel_settings = array(
  'style_settings' => array(
    'default' => NULL,
    'center' => NULL,
  ),
);
$display->cache = array();
$display->title = 'Terms of Use';
$display->uuid = '7ade25a8-14ce-4274-8a3a-a53753311657';
$display->content = array();
$display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-6c031bbb-f887-48a1-8d88-0cebbbbca9a5';
  $pane->panel = 'center';
  $pane->type = 'custom';
  $pane->subtype = 'custom';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'admin_title' => 'Terms body',
    'title' => '',
    'body' => '<script>
(function ($) {
$( document ).ready(function() {
    $( "#load-text" ).load( "../index/terms.php #legal-pan" );
});
})(jQuery);

</script>
<div id="load-text"></div>',
    'format' => 'full_html',
    'substitute' => TRUE,
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array(
    'css_id' => '',
    'css_class' => 'body-style',
  );
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '6c031bbb-f887-48a1-8d88-0cebbbbca9a5';
  $display->content['new-6c031bbb-f887-48a1-8d88-0cebbbbca9a5'] = $pane;
  $display->panels['center'][0] = 'new-6c031bbb-f887-48a1-8d88-0cebbbbca9a5';
  $pane = new stdClass();
  $pane->pid = 'new-b477a520-46d0-4823-ac6f-eac18ed628a9';
  $pane->panel = 'center';
  $pane->type = 'custom';
  $pane->subtype = 'custom';
  $pane->shown = TRUE;
  $pane->access = array(
    'plugins' => array(
      0 => array(
        'name' => 'theme',
        'settings' => array(
          'theme' => 'thebulletin',
        ),
        'not' => FALSE,
      ),
    ),
  );
  $pane->configuration = array(
    'admin_title' => 'Css Back',
    'title' => '',
    'body' => '<style>
#back-btn2 {
	display: block;
	width: 35px;
	float: left;
	height: 35px;
	background: url(\'http://mv.local/oprsndo/themecore/back-btn.png\') center 7px / 20px 20px no-repeat;
	margin-top: 25px;
}

#back-btn {
	display: none;
}
</style>',
    'format' => 'full_html',
    'substitute' => TRUE,
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 1;
  $pane->locks = array();
  $pane->uuid = 'b477a520-46d0-4823-ac6f-eac18ed628a9';
  $display->content['new-b477a520-46d0-4823-ac6f-eac18ed628a9'] = $pane;
  $display->panels['center'][1] = 'new-b477a520-46d0-4823-ac6f-eac18ed628a9';
$display->hide_title = PANELS_TITLE_FIXED;
$display->title_pane = 'new-6c031bbb-f887-48a1-8d88-0cebbbbca9a5';
$handler->conf['display'] = $display;
$page->default_handlers[$handler->name] = $handler;
