<?php

$page = new stdClass();
$page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
$page->api_version = 1;
$page->name = 'dashboard';
$page->task = 'page';
$page->admin_title = 'Dashboard';
$page->admin_description = 'User Dashboard';
$page->path = 'dashboard';
$page->access = array();
$page->menu = array();
$page->arguments = array();
$page->conf = array(
  'admin_paths' => FALSE,
);
$page->default_handlers = array();
$handler = new stdClass();
$handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
$handler->api_version = 1;
$handler->name = 'page_dashboard_panel_context_2';
$handler->task = 'page';
$handler->subtask = 'dashboard';
$handler->handler = 'panel_context';
$handler->weight = -30;
$handler->conf = array(
  'title' => 'Dashboard',
  'no_blocks' => 0,
  'pipeline' => 'standard',
  'body_classes_to_remove' => '',
  'body_classes_to_add' => 'dashboard',
  'css_id' => '',
  'css' => '',
  'contexts' => array(
    0 => array(
      'identifier' => 'User',
      'keyword' => 'user',
      'name' => 'user',
      'type' => 'current',
      'uid' => '',
      'id' => 1,
    ),
  ),
  'relationships' => array(),
  'access' => array(
    'plugins' => array(
      0 => array(
        'name' => 'role',
        'settings' => array(
          'rids' => array(
            0 => 84425795,
          ),
        ),
        'context' => 'context_user_1',
        'not' => FALSE,
      ),
    ),
    'logic' => 'and',
  ),
);
$display = new panels_display();
$display->layout = 'onecol';
$display->layout_settings = array();
$display->panel_settings = array(
  'style_settings' => array(
    'default' => NULL,
    'middle' => NULL,
  ),
);
$display->cache = array();
$display->title = '';
$display->uuid = '199a5b14-885b-4339-ba1e-8fa2532cffcf';
$display->content = array();
$display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-b4dea463-b100-4b94-acbf-7ec2a9360730';
  $pane->panel = 'middle';
  $pane->type = 'views';
  $pane->subtype = 'bulletin';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_pager_settings' => 0,
    'use_pager' => 0,
    'nodes_per_page' => '0',
    'pager_id' => '0',
    'offset' => '0',
    'more_link' => 0,
    'feed_icons' => 0,
    'panel_args' => 0,
    'link_to_view' => 0,
    'args' => '',
    'url' => '',
    'display' => 'location_search',
    'context' => array(
      0 => '',
    ),
    'override_title' => 0,
    'override_title_text' => '',
    'override_title_heading' => 'h2',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array(
    'css_id' => '',
    'css_class' => 'location-search',
  );
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = 'b4dea463-b100-4b94-acbf-7ec2a9360730';
  $display->content['new-b4dea463-b100-4b94-acbf-7ec2a9360730'] = $pane;
  $display->panels['middle'][0] = 'new-b4dea463-b100-4b94-acbf-7ec2a9360730';
  $pane = new stdClass();
  $pane->pid = 'new-90c31873-0188-4550-9c5f-ca4adccab5ec';
  $pane->panel = 'middle';
  $pane->type = 'custom';
  $pane->subtype = 'custom';
  $pane->shown = TRUE;
  $pane->access = array(
    'plugins' => array(
      0 => array(
        'name' => 'role',
        'settings' => array(
          'rids' => array(
            0 => 170434716,
          ),
        ),
        'context' => 'logged-in-user',
        'not' => FALSE,
      ),
    ),
  );
  $pane->configuration = array(
    'admin_title' => 'Upgrade Hover JS & Boxes',
    'title' => '',
    'body' => '<h1>Your trial has ended</h1>
<h2>We hope you enjoyed and will consider upgrading to take advantage of the many features Dispatch has to offer.</h2>

<div id="upgrade-close">Continue using Dispatch in a limited capacity.</div>
<script>
(function ($) {

$(document).ready(function() {
    if ($.cookie("upgradeNotice")) $(".upgrade-hover-top, .upgrade-hover-box").hide();
    else {
        $(".upgrade-hover").delay( 800 ).fadeIn("slow");
        $.cookie(\'upgradeNotice\', false);
    }
});
})(jQuery);

</script>',
    'format' => 'full_html',
    'substitute' => 1,
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array(
    'css_id' => '',
    'css_class' => 'upgrade-hover-top',
  );
  $pane->extras = array();
  $pane->position = 1;
  $pane->locks = array();
  $pane->uuid = '90c31873-0188-4550-9c5f-ca4adccab5ec';
  $display->content['new-90c31873-0188-4550-9c5f-ca4adccab5ec'] = $pane;
  $display->panels['middle'][1] = 'new-90c31873-0188-4550-9c5f-ca4adccab5ec';
  $pane = new stdClass();
  $pane->pid = 'new-1fa72a57-bdfa-42d3-bce4-cff845f8e245';
  $pane->panel = 'middle';
  $pane->type = 'panels_mini';
  $pane->subtype = 'reporter_upgrade_box';
  $pane->shown = TRUE;
  $pane->access = array(
    'plugins' => array(
      0 => array(
        'name' => 'role',
        'settings' => array(
          'rids' => array(
            0 => 170434716,
          ),
        ),
        'context' => 'logged-in-user',
        'not' => FALSE,
      ),
    ),
  );
  $pane->configuration = array(
    'override_title' => 0,
    'override_title_text' => '',
    'override_title_heading' => 'h2',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array(
    'css_id' => '',
    'css_class' => 'upgrade-hover-box',
  );
  $pane->extras = array();
  $pane->position = 2;
  $pane->locks = array();
  $pane->uuid = '1fa72a57-bdfa-42d3-bce4-cff845f8e245';
  $display->content['new-1fa72a57-bdfa-42d3-bce4-cff845f8e245'] = $pane;
  $display->panels['middle'][2] = 'new-1fa72a57-bdfa-42d3-bce4-cff845f8e245';
  $pane = new stdClass();
  $pane->pid = 'new-4f2d4d95-d6d6-491c-8239-0c9accddfdfd';
  $pane->panel = 'middle';
  $pane->type = 'custom';
  $pane->subtype = 'custom';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'admin_title' => 'Message Count',
    'title' => '',
    'body' => '<div class="badge" id="unread_message-count">
<?php
print $inbox=privatemsg_unread_count($account = NULL);
?>
</div>',
    'format' => 'php_code',
    'substitute' => 1,
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 3;
  $pane->locks = array();
  $pane->uuid = '4f2d4d95-d6d6-491c-8239-0c9accddfdfd';
  $display->content['new-4f2d4d95-d6d6-491c-8239-0c9accddfdfd'] = $pane;
  $display->panels['middle'][3] = 'new-4f2d4d95-d6d6-491c-8239-0c9accddfdfd';
  $pane = new stdClass();
  $pane->pid = 'new-0fcf2ed4-4da7-444a-aede-aea5d7150bd9';
  $pane->panel = 'middle';
  $pane->type = 'custom';
  $pane->subtype = 'custom';
  $pane->shown = TRUE;
  $pane->access = array(
    'plugins' => array(),
  );
  $pane->configuration = array(
    'admin_title' => 'Map Btns - SaaS',
    'title' => '',
    'body' => '<div class="map-pan-icon" id="activity-btn"></div>
<div class="map-pan-icon" id="pan-dispatch"></div>
<div class="map-pan-icon" id="pan-notes"></div>
<a href="?q=messages/inbox" class="map-pan-icon" id="pan-mess" target="node-embed-frame"></a>',
    'format' => 'full_html',
    'substitute' => 0,
  );
  $pane->cache = array(
    'method' => 'simple',
    'settings' => array(
      'lifetime' => '604800',
      'granularity' => 'none',
    ),
  );
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array(
    'css_id' => '',
    'css_class' => 'map-pan-nav',
  );
  $pane->extras = array();
  $pane->position = 4;
  $pane->locks = array();
  $pane->uuid = '0fcf2ed4-4da7-444a-aede-aea5d7150bd9';
  $display->content['new-0fcf2ed4-4da7-444a-aede-aea5d7150bd9'] = $pane;
  $display->panels['middle'][4] = 'new-0fcf2ed4-4da7-444a-aede-aea5d7150bd9';
  $pane = new stdClass();
  $pane->pid = 'new-d2eea973-9fbf-4c5c-a38f-7dac4dc0da6b';
  $pane->panel = 'middle';
  $pane->type = 'views';
  $pane->subtype = 'bulletin';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_pager_settings' => 0,
    'use_pager' => 0,
    'nodes_per_page' => '0',
    'pager_id' => '0',
    'offset' => '0',
    'more_link' => 0,
    'feed_icons' => 0,
    'panel_args' => 0,
    'link_to_view' => 0,
    'args' => '',
    'url' => '',
    'display' => 'block_10',
    'context' => array(
      0 => '',
    ),
    'override_title' => 1,
    'override_title_text' => '',
    'override_title_heading' => 'h2',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array(
    'css_id' => 'dash-side-activity',
    'css_class' => 'side-activity',
  );
  $pane->extras = array();
  $pane->position = 5;
  $pane->locks = array();
  $pane->uuid = 'd2eea973-9fbf-4c5c-a38f-7dac4dc0da6b';
  $display->content['new-d2eea973-9fbf-4c5c-a38f-7dac4dc0da6b'] = $pane;
  $display->panels['middle'][5] = 'new-d2eea973-9fbf-4c5c-a38f-7dac4dc0da6b';
  $pane = new stdClass();
  $pane->pid = 'new-1258d5dc-6148-423f-a11e-5899defb1647';
  $pane->panel = 'middle';
  $pane->type = 'panels_mini';
  $pane->subtype = 'dispatch';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => 1,
    'override_title_text' => '',
    'override_title_heading' => 'h2',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array(
    'css_id' => 'dash-slide-box-dispatch',
    'css_class' => 'dash-slide-dispatch',
  );
  $pane->extras = array();
  $pane->position = 6;
  $pane->locks = array();
  $pane->uuid = '1258d5dc-6148-423f-a11e-5899defb1647';
  $display->content['new-1258d5dc-6148-423f-a11e-5899defb1647'] = $pane;
  $display->panels['middle'][6] = 'new-1258d5dc-6148-423f-a11e-5899defb1647';
  $pane = new stdClass();
  $pane->pid = 'new-44c43f72-ef29-4a42-99e4-7cf360c7f072';
  $pane->panel = 'middle';
  $pane->type = 'custom';
  $pane->subtype = 'custom';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'admin_title' => 'Dispatch List Iframe',
    'title' => '',
    'body' => '<span class="list-close">x</span>
<iframe src="" name="dispatch-list"></iframe>',
    'format' => 'full_html',
    'substitute' => 1,
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array(
    'css_id' => 'dispatch-list-frame',
    'css_class' => 'dispatch-list-main',
  );
  $pane->extras = array();
  $pane->position = 7;
  $pane->locks = array();
  $pane->uuid = '44c43f72-ef29-4a42-99e4-7cf360c7f072';
  $display->content['new-44c43f72-ef29-4a42-99e4-7cf360c7f072'] = $pane;
  $display->panels['middle'][7] = 'new-44c43f72-ef29-4a42-99e4-7cf360c7f072';
  $pane = new stdClass();
  $pane->pid = 'new-3db70ad4-cea5-4bc0-ad4a-a7c78a9b5603';
  $pane->panel = 'middle';
  $pane->type = 'panels_mini';
  $pane->subtype = 'micro_note';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => 0,
    'override_title_text' => '',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array(
    'css_id' => 'dash-slide-box',
    'css_class' => 'dash-slide-notes',
  );
  $pane->extras = array();
  $pane->position = 8;
  $pane->locks = array();
  $pane->uuid = '3db70ad4-cea5-4bc0-ad4a-a7c78a9b5603';
  $display->content['new-3db70ad4-cea5-4bc0-ad4a-a7c78a9b5603'] = $pane;
  $display->panels['middle'][8] = 'new-3db70ad4-cea5-4bc0-ad4a-a7c78a9b5603';
  $pane = new stdClass();
  $pane->pid = 'new-9d10d323-a6ee-4959-9caf-d08508dc6a60';
  $pane->panel = 'middle';
  $pane->type = 'custom';
  $pane->subtype = 'custom';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'admin_title' => 'Map',
    'title' => '',
    'body' => '<iframe src="?q=tweet-map" id="iframe" name="iframe" class="map"></iframe>',
    'format' => 'full_html',
    'substitute' => 1,
  );
  $pane->cache = array(
    'method' => 'simple',
    'settings' => array(
      'lifetime' => '604800',
      'granularity' => 'none',
    ),
  );
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array(
    'css_id' => '',
    'css_class' => 'map-box',
  );
  $pane->extras = array();
  $pane->position = 9;
  $pane->locks = array();
  $pane->uuid = '9d10d323-a6ee-4959-9caf-d08508dc6a60';
  $display->content['new-9d10d323-a6ee-4959-9caf-d08508dc6a60'] = $pane;
  $display->panels['middle'][9] = 'new-9d10d323-a6ee-4959-9caf-d08508dc6a60';
  $pane = new stdClass();
  $pane->pid = 'new-30fdd6a1-3829-49de-a0bd-9cfd1e431e16';
  $pane->panel = 'middle';
  $pane->type = 'custom';
  $pane->subtype = 'custom';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'admin_title' => 'Cookie Items',
    'title' => '',
    'body' => '<script>
(function ($) {

$(document).ready(function() {
	if ($.cookie("weclomeScreen")) $("#block-block-4").hide();
	else {
	    $("#block-block-4").delay( 800 ).fadeIn("slow");
		$("#block-block-4").click(function () {
			$("#block-block-4").fadeOut();
		});
	    $.cookie("weclomeScreen", false);
	}
});
})(jQuery);

</script>',
    'format' => 'full_html',
    'substitute' => 1,
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 10;
  $pane->locks = array();
  $pane->uuid = '30fdd6a1-3829-49de-a0bd-9cfd1e431e16';
  $display->content['new-30fdd6a1-3829-49de-a0bd-9cfd1e431e16'] = $pane;
  $display->panels['middle'][10] = 'new-30fdd6a1-3829-49de-a0bd-9cfd1e431e16';
  $pane = new stdClass();
  $pane->pid = 'new-ea6ff9aa-e28c-419a-b61e-5c613854590e';
  $pane->panel = 'middle';
  $pane->type = 'custom';
  $pane->subtype = 'custom';
  $pane->shown = TRUE;
  $pane->access = array(
    'plugins' => array(
      0 => array(
        'name' => 'role',
        'settings' => array(
          'rids' => array(
            0 => 225332102,
            1 => 172216448,
            2 => 185875538,
            3 => 40207975,
          ),
        ),
        'context' => 'context_user_1',
        'not' => FALSE,
      ),
    ),
  );
  $pane->configuration = array(
    'admin_title' => 'Share Bulletin',
    'title' => '',
    'body' => '<div id="share-wraper" class="share-shell">
<div class="share-header"><div class="share-icon"></div>Discover Bulletin</div>

<div class="share-caption">Bulletin, is our mobile app that powers our platform. <br />Please help us spread the word so more people can report unique stories.</div>
<a href="https://www.openreporter.org/?q=bulletin" target"_blank" class="btn-red">Learn More</a>

</div>',
    'format' => 'full_html',
    'substitute' => 1,
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 11;
  $pane->locks = array();
  $pane->uuid = 'ea6ff9aa-e28c-419a-b61e-5c613854590e';
  $display->content['new-ea6ff9aa-e28c-419a-b61e-5c613854590e'] = $pane;
  $display->panels['middle'][11] = 'new-ea6ff9aa-e28c-419a-b61e-5c613854590e';
  $pane = new stdClass();
  $pane->pid = 'new-e8414c86-761e-4a32-b094-aa57046e381c';
  $pane->panel = 'middle';
  $pane->type = 'custom';
  $pane->subtype = 'custom';
  $pane->shown = TRUE;
  $pane->access = array(
    'plugins' => array(
      0 => array(
        'name' => 'role',
        'settings' => array(
          'rids' => array(
            0 => 170434716,
          ),
        ),
        'context' => 'context_user_1',
        'not' => FALSE,
      ),
    ),
  );
  $pane->configuration = array(
    'admin_title' => 'Upgrade Pan',
    'title' => '',
    'body' => '<div id="share-wraper" class="share-shell">
<div class="share-header"><div class="share-icon"></div>Upgrade Account</div>

<div class="share-caption">View more stories, track your findings and reference up to two weeks of past events. <br />Upgrade to get the most out of Dispatch.</div>
<a href="?q=upgrade" class="btn-red">Get Started</a>

</div>',
    'format' => 'full_html',
    'substitute' => 1,
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 12;
  $pane->locks = array();
  $pane->uuid = 'e8414c86-761e-4a32-b094-aa57046e381c';
  $display->content['new-e8414c86-761e-4a32-b094-aa57046e381c'] = $pane;
  $display->panels['middle'][12] = 'new-e8414c86-761e-4a32-b094-aa57046e381c';
  $pane = new stdClass();
  $pane->pid = 'new-e63b50d5-b290-48fa-a1ca-01b7797a5518';
  $pane->panel = 'middle';
  $pane->type = 'custom';
  $pane->subtype = 'custom';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'admin_title' => 'Mailbox',
    'title' => '',
    'body' => '<div class="close" id="node-frame-close">x</div>
<iframe src="" name="node-embed-frame" id="node-frame-load"></iframe>',
    'format' => 'full_html',
    'substitute' => 1,
  );
  $pane->cache = array(
    'method' => 'simple',
    'settings' => array(
      'lifetime' => '604800',
      'granularity' => 'none',
    ),
  );
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array(
    'css_id' => 'node-frame',
    'css_class' => '',
  );
  $pane->extras = array();
  $pane->position = 13;
  $pane->locks = array();
  $pane->uuid = 'e63b50d5-b290-48fa-a1ca-01b7797a5518';
  $display->content['new-e63b50d5-b290-48fa-a1ca-01b7797a5518'] = $pane;
  $display->panels['middle'][13] = 'new-e63b50d5-b290-48fa-a1ca-01b7797a5518';
  $pane = new stdClass();
  $pane->pid = 'new-eabefd58-ee03-4ab0-b63f-46f9679195b4';
  $pane->panel = 'middle';
  $pane->type = 'custom';
  $pane->subtype = 'custom';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'admin_title' => 'Birds Eye',
    'title' => '',
    'body' => '<div class="close-btn" id="bird-eye-close">x</div>
<form action="s/scrape.php" target="scrape-frame" method="post" class="hide">
	<input type="text" name="url-name" id="url-name">
	<input id="url-name-btn" type="submit">
</form>
<div class="tweet-icon-bi"></div>
<iframe name="scrape-frame" id="scrape-frame" src="s/scrape.php"></iframe>

<div id="in"></div>
<div id="out"></div>

<!-- PHP Feed -->
<form action="s/feed.php" target="feed-frame" method="post" class="hide">
	<input type="text" name="google-loc" id="google-loc">
	<input id="feed-btn" type="submit">
</form>
<div class="goog-icon-bi"></div>
<iframe name="feed-frame" id="feed-frame" src="s/feed.php"></iframe>',
    'format' => 'full_html',
    'substitute' => 1,
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array(
    'css_id' => '',
    'css_class' => 'birds-eye',
  );
  $pane->extras = array();
  $pane->position = 14;
  $pane->locks = array();
  $pane->uuid = 'eabefd58-ee03-4ab0-b63f-46f9679195b4';
  $display->content['new-eabefd58-ee03-4ab0-b63f-46f9679195b4'] = $pane;
  $display->panels['middle'][14] = 'new-eabefd58-ee03-4ab0-b63f-46f9679195b4';
  $pane = new stdClass();
  $pane->pid = 'new-04d799dd-1bf1-4784-aa5c-fcf60321a70d';
  $pane->panel = 'middle';
  $pane->type = 'custom';
  $pane->subtype = 'custom';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'admin_title' => 'Node Frame',
    'title' => '',
    'body' => '<div class="close-btn" id="node-frame-close">x</div>
<iframe name="boo" id="boo-frame">
</iframe>',
    'format' => 'full_html',
    'substitute' => 1,
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array(
    'css_id' => 'node-load',
    'css_class' => '',
  );
  $pane->extras = array();
  $pane->position = 15;
  $pane->locks = array();
  $pane->uuid = '04d799dd-1bf1-4784-aa5c-fcf60321a70d';
  $display->content['new-04d799dd-1bf1-4784-aa5c-fcf60321a70d'] = $pane;
  $display->panels['middle'][15] = 'new-04d799dd-1bf1-4784-aa5c-fcf60321a70d';
$display->hide_title = PANELS_TITLE_NONE;
$display->title_pane = '0';
$handler->conf['display'] = $display;
$page->default_handlers[$handler->name] = $handler;
$handler = new stdClass();
$handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
$handler->api_version = 1;
$handler->name = 'page_dashboard_panel_context_3';
$handler->task = 'page';
$handler->subtask = 'dashboard';
$handler->handler = 'panel_context';
$handler->weight = -28;
$handler->conf = array(
  'title' => 'Front',
  'no_blocks' => 0,
  'pipeline' => 'standard',
  'body_classes_to_remove' => '',
  'body_classes_to_add' => '',
  'css_id' => '',
  'css' => '',
  'contexts' => array(),
  'relationships' => array(),
  'access' => array(
    'plugins' => array(
      0 => array(
        'name' => 'role',
        'settings' => array(
          'rids' => array(
            0 => 1,
          ),
        ),
        'context' => 'logged-in-user',
        'not' => FALSE,
      ),
    ),
    'logic' => 'and',
  ),
  'metatag_panels' => array(
    'enabled' => 1,
    'metatags' => array(
      'title' => array(
        'value' => 'Openreporter - A platform to revitalize the news.',
      ),
      'description' => array(
        'value' => 'Helping journalists produce informative stories, citizens raise awareness, and experts voice their expertise.',
      ),
      'keywords' => array(
        'value' => 'Collaborative Journalism, Social Network, Social Network for Journalists, Platform, Journalism, News, Resources, Reporters, Network, Jobs, Reporter, professional',
      ),
    ),
  ),
);
$display = new panels_display();
$display->layout = 'onecol';
$display->layout_settings = array();
$display->panel_settings = array(
  'style_settings' => array(
    'default' => NULL,
    'center' => NULL,
    'middle' => NULL,
  ),
);
$display->cache = array();
$display->title = '';
$display->uuid = 'df366e7e-90ad-4cee-8dfb-6eb58f2c30c0';
$display->content = array();
$display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-a48b3618-c9e0-47d3-b3a0-5a4200ccb3aa';
  $pane->panel = 'middle';
  $pane->type = 'custom';
  $pane->subtype = 'custom';
  $pane->shown = TRUE;
  $pane->access = array(
    'plugins' => array(
      0 => array(
        'name' => 'theme',
        'settings' => array(
          'theme' => 'thebulletin',
        ),
        'not' => FALSE,
      ),
    ),
  );
  $pane->configuration = array(
    'admin_title' => 'Welcome Bubble',
    'title' => '',
    'body' => '<div class="navigation-bar text-right"><a href="?q=citizen/register/" class="welcome-page">Sign Up</a></div>
<div class="welcome-bubble"></div>',
    'format' => 'full_html',
    'substitute' => TRUE,
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array(
    'css_id' => 'wd',
    'css_class' => '',
  );
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = 'a48b3618-c9e0-47d3-b3a0-5a4200ccb3aa';
  $display->content['new-a48b3618-c9e0-47d3-b3a0-5a4200ccb3aa'] = $pane;
  $display->panels['middle'][0] = 'new-a48b3618-c9e0-47d3-b3a0-5a4200ccb3aa';
  $pane = new stdClass();
  $pane->pid = 'new-8c2759b1-02be-4717-879c-b53155acd473';
  $pane->panel = 'middle';
  $pane->type = 'custom';
  $pane->subtype = 'custom';
  $pane->shown = TRUE;
  $pane->access = array(
    'plugins' => array(
      0 => array(
        'name' => 'theme',
        'settings' => array(
          'theme' => 'thebulletin',
        ),
        'not' => FALSE,
      ),
    ),
  );
  $pane->configuration = array(
    'admin_title' => 'App: Informative Welcome Screen',
    'title' => '',
    'body' => '<div id="wrap-shell">
<link rel="stylesheet" href="//mv.local/themecore/style-welcome.css"/>
<a href="opr-location://" id="location-hide">test</a>
<div id="shell">
    <ul>
      <li class="dragend-page" id="slide1">
			<div class="h1">Welcome</div>
			<div class="slide-img" id="slide-img-bubble"></div>
			<div class="slide-dots"><span class="dot-active">.</span>.....</div>
			<div class="slide-body-text">
			Bulletin allows you to report events directly to journalists.</div>
      </li>
      <li class="dragend-page" id="slide2">
			<div class="h1">How does it work?</div>
			<div class="slide-img" id="slide-img-phone"></div>
			<div class="slide-dots">.<span class="dot-active">.</span>...</div>
			<div class="slide-body-text">When you witness an event, use the app to report it.</div>
      </li>
      <li class="dragend-page" id="slide3">
			<div class="h1">Answer Questions</div>
			<div class="slide-img" id="slide-img-message"></div>
			<div class="slide-dots">..<span class="dot-active">.</span>...</div>
			<div class="slide-body-text">Journalists will see your story and message you with questions.</div>
      </li>
      <li class="dragend-page" id="slide4">
			<div class="h1">What to report?</div>
			<div class="slide-img" id="slide-img-report"></div>
			<div class="slide-dots">...<span class="dot-active">.</span>..</div>
			<div class="slide-body-text">Your day is full of interesting stories. So report what you see.</div>
      </li>
      <li class="dragend-page" id="slide5">
			<div class="h1">Location</div>
			<div class="slide-img" id="slide-img-location"></div>
			<div class="slide-dots">....<span class="dot-active">.</span>.</div>
			<div class="slide-body-text">Location is required to help establish validity for your reports. </div>
      </li>
      <li class="dragend-page" id="slide6">
			<div class="h1">Start Reporting!</div>
			<div class="slide-img" id="slide-img-checkmark"></div>
                        <div class="slide-dots">.....<span class="dot-active">.</span></div>
			<div class="slide-body-text">Next you will need to login or create an account.</div>
      </li>
    </ul>
  </div>

<script type="text/javascript" src="//mv.local/themecore/jquery.min.js"></script>
<script type="text/javascript" src="//mv.local/themecore/dragend.js"></script>
<script>
(function ($) {
	$(function() {
	$("#shell").dragend({
	afterInitialize: function() {
	  this.container.style.visibility = "visible";
	},
	onSwipeEnd: function() {
	  var first = this.pages[0],
	      last = this.pages[this.pages.length - 1];
	  $(".dragend-page").removeClass("active");
	  $(".dragend-page").eq(this.page).addClass("active");

	  if ($("#slide6").hasClass("active")) {
	  	$(\'#location-hide\')[0].click();
	  }
	}
	});
        $(".slide-btn").click(function(){
	       $(\'#location-hide\')[0].click();
        });
        $("#slide-close").click(function(){
		$("#wrap-shell").fadeOut();
                $("div.pane-content h1, .app-login-box, .reg-block").fadeIn();
	});
	});
})(jQuery);
</script>
<div class="splash-footer">
	<a href="?q=citizen/register" target="_parent" class="slide-btn" id="create-acct">Create an account</a>
	<div class="slide-btn" id="slide-close">Login</div>
</div>
</div>',
    'format' => 'full_html',
    'substitute' => TRUE,
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array(
    'css_id' => 'welcome-screen',
    'css_class' => '',
  );
  $pane->extras = array();
  $pane->position = 1;
  $pane->locks = array();
  $pane->uuid = '8c2759b1-02be-4717-879c-b53155acd473';
  $display->content['new-8c2759b1-02be-4717-879c-b53155acd473'] = $pane;
  $display->panels['middle'][1] = 'new-8c2759b1-02be-4717-879c-b53155acd473';
  $pane = new stdClass();
  $pane->pid = 'new-b606722b-7a15-4172-a2e3-5ac26beb2a19';
  $pane->panel = 'middle';
  $pane->type = 'page_messages';
  $pane->subtype = 'page_messages';
  $pane->shown = TRUE;
  $pane->access = array(
    'plugins' => array(
      0 => array(
        'name' => 'theme',
        'settings' => array(
          'theme' => 'thebulletin',
        ),
        'not' => FALSE,
      ),
    ),
  );
  $pane->configuration = array();
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 2;
  $pane->locks = array();
  $pane->uuid = 'b606722b-7a15-4172-a2e3-5ac26beb2a19';
  $display->content['new-b606722b-7a15-4172-a2e3-5ac26beb2a19'] = $pane;
  $display->panels['middle'][2] = 'new-b606722b-7a15-4172-a2e3-5ac26beb2a19';
  $pane = new stdClass();
  $pane->pid = 'new-b764ada3-f559-43a1-992d-7a61a8ae8fbe';
  $pane->panel = 'middle';
  $pane->type = 'block';
  $pane->subtype = 'user-login';
  $pane->shown = TRUE;
  $pane->access = array(
    'plugins' => array(),
  );
  $pane->configuration = array(
    'override_title' => 1,
    'override_title_text' => '',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array(
    'css_id' => '',
    'css_class' => 'app-login-box',
  );
  $pane->extras = array();
  $pane->position = 3;
  $pane->locks = array();
  $pane->uuid = 'b764ada3-f559-43a1-992d-7a61a8ae8fbe';
  $display->content['new-b764ada3-f559-43a1-992d-7a61a8ae8fbe'] = $pane;
  $display->panels['middle'][3] = 'new-b764ada3-f559-43a1-992d-7a61a8ae8fbe';
  $pane = new stdClass();
  $pane->pid = 'new-00cc0920-bc13-457e-8b46-b16e7ef16b82';
  $pane->panel = 'middle';
  $pane->type = 'custom';
  $pane->subtype = 'custom';
  $pane->shown = TRUE;
  $pane->access = array(
    'plugins' => array(
      0 => array(
        'name' => 'theme',
        'settings' => array(
          'theme' => 'new_front',
        ),
        'not' => FALSE,
      ),
    ),
  );
  $pane->configuration = array(
    'admin_title' => 'Register/Password (Reporter)',
    'title' => '',
    'body' => '<a href="?q=reporter/register" class="register">Register</a>',
    'format' => 'full_html',
    'substitute' => TRUE,
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 4;
  $pane->locks = array();
  $pane->uuid = '00cc0920-bc13-457e-8b46-b16e7ef16b82';
  $display->content['new-00cc0920-bc13-457e-8b46-b16e7ef16b82'] = $pane;
  $display->panels['middle'][4] = 'new-00cc0920-bc13-457e-8b46-b16e7ef16b82';
  $pane = new stdClass();
  $pane->pid = 'new-13df4458-581c-4a60-b230-00dbe9ad1903';
  $pane->panel = 'middle';
  $pane->type = 'custom';
  $pane->subtype = 'custom';
  $pane->shown = TRUE;
  $pane->access = array(
    'plugins' => array(
      0 => array(
        'name' => 'theme',
        'settings' => array(
          'theme' => 'thebulletin',
        ),
        'not' => FALSE,
      ),
    ),
  );
  $pane->configuration = array(
    'admin_title' => 'Mobile App Rest Link',
    'title' => '',
    'body' => '<a href="//account.openreporter.org/?q=user/password" id="reset-link">Request new password</a>',
    'format' => 'filtered_html',
    'substitute' => TRUE,
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 5;
  $pane->locks = array();
  $pane->uuid = '13df4458-581c-4a60-b230-00dbe9ad1903';
  $display->content['new-13df4458-581c-4a60-b230-00dbe9ad1903'] = $pane;
  $display->panels['middle'][5] = 'new-13df4458-581c-4a60-b230-00dbe9ad1903';
$display->hide_title = PANELS_TITLE_NONE;
$display->title_pane = '0';
$handler->conf['display'] = $display;
$page->default_handlers[$handler->name] = $handler;
$handler = new stdClass();
$handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
$handler->api_version = 1;
$handler->name = 'page_dashboard_panel_context';
$handler->task = 'page';
$handler->subtask = 'dashboard';
$handler->handler = 'panel_context';
$handler->weight = -27;
$handler->conf = array(
  'title' => 'Citizen Dashboard',
  'no_blocks' => 0,
  'pipeline' => 'standard',
  'body_classes_to_remove' => '',
  'body_classes_to_add' => '',
  'css_id' => '',
  'css' => '',
  'contexts' => array(),
  'relationships' => array(),
  'access' => array(
    'plugins' => array(
      0 => array(
        'name' => 'role',
        'settings' => array(
          'rids' => array(
            0 => 226993263,
          ),
        ),
        'context' => 'logged-in-user',
        'not' => FALSE,
      ),
      1 => array(
        'name' => 'role',
        'settings' => array(
          'rids' => array(
            0 => 84425795,
          ),
        ),
        'context' => 'logged-in-user',
        'not' => TRUE,
      ),
    ),
    'logic' => 'and',
  ),
  'metatag_panels' => array(
    'enabled' => 0,
    'metatags' => array(),
  ),
);
$display = new panels_display();
$display->layout = 'onecol';
$display->layout_settings = array();
$display->panel_settings = array(
  'style_settings' => array(
    'default' => NULL,
    'middle' => NULL,
  ),
);
$display->cache = array();
$display->title = '';
$display->uuid = 'fd816ecc-a252-48c9-90a7-7ecd21c81ef4';
$display->content = array();
$display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-7d5e3168-29aa-4b43-bd8d-dbc9b697cb76';
  $pane->panel = 'middle';
  $pane->type = 'custom';
  $pane->subtype = 'custom';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'admin_title' => 'Main Menu',
    'title' => '',
    'body' => '<div class="navigation-bar main-bar-bg">	
	<div class="main-bar-items">
		<a  href="?q=node/add/bulletin-source" id="post" target="white-frame"></a>
		
		<div class="bul-logo" id="bubble-top"></div>	
		<div class="right-blox">
			<ul>
				<li class="nav-icon"><a href="?q=dispatch" id="req-icon" target="darkbl-frame"></a></li>
				<li class="nav-icon"><div id="settings-icon"></div></li>
			</ul>
		</div>
	</div>
</div>',
    'format' => 'full_html',
    'substitute' => TRUE,
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '7d5e3168-29aa-4b43-bd8d-dbc9b697cb76';
  $display->content['new-7d5e3168-29aa-4b43-bd8d-dbc9b697cb76'] = $pane;
  $display->panels['middle'][0] = 'new-7d5e3168-29aa-4b43-bd8d-dbc9b697cb76';
  $pane = new stdClass();
  $pane->pid = 'new-5256bea9-6fbe-4c71-b929-d4dc7c63ce0c';
  $pane->panel = 'middle';
  $pane->type = 'panels_mini';
  $pane->subtype = 'profile_banner';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => 0,
    'override_title_text' => '',
    'override_title_heading' => 'h2',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array(
    'css_id' => 'profile-banner',
    'css_class' => 'body-cell body-top',
  );
  $pane->extras = array();
  $pane->position = 1;
  $pane->locks = array();
  $pane->uuid = '5256bea9-6fbe-4c71-b929-d4dc7c63ce0c';
  $display->content['new-5256bea9-6fbe-4c71-b929-d4dc7c63ce0c'] = $pane;
  $display->panels['middle'][1] = 'new-5256bea9-6fbe-4c71-b929-d4dc7c63ce0c';
  $pane = new stdClass();
  $pane->pid = 'new-89264bfe-9d87-4860-9709-7aef773ca203';
  $pane->panel = 'middle';
  $pane->type = 'views';
  $pane->subtype = 'bulletin';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_pager_settings' => 0,
    'use_pager' => 1,
    'nodes_per_page' => '15',
    'pager_id' => '0',
    'offset' => '0',
    'more_link' => 0,
    'feed_icons' => 0,
    'panel_args' => 0,
    'link_to_view' => 0,
    'args' => '',
    'url' => '',
    'display' => 'block',
    'override_title' => 1,
    'override_title_text' => '',
    'override_title_heading' => 'h2',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array(
    'css_id' => 'dashboard-main',
    'css_class' => 'body-ajax',
  );
  $pane->extras = array();
  $pane->position = 2;
  $pane->locks = array();
  $pane->uuid = '89264bfe-9d87-4860-9709-7aef773ca203';
  $display->content['new-89264bfe-9d87-4860-9709-7aef773ca203'] = $pane;
  $display->panels['middle'][2] = 'new-89264bfe-9d87-4860-9709-7aef773ca203';
  $pane = new stdClass();
  $pane->pid = 'new-e880d1c5-5691-46fb-aae1-bf75820cc608';
  $pane->panel = 'middle';
  $pane->type = 'custom';
  $pane->subtype = 'custom';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'admin_title' => 'Slide Pans',
    'title' => '',
    'body' => '<div id="white-view" class="white-slide slide-pan">
<div id="load"></div>		
<iframe name="white-frame" id="white-frame"></iframe>
</div>

<form>
  <input type="text" id="lat">
  <input type="text" id="long">
</form>

<div id="second-nav" class="darkbl-slide slide-pan">
<div id="load"></div>		
<iframe name="darkbl-frame" id="darkbl-frame"></iframe>
</div>',
    'format' => 'full_html',
    'substitute' => TRUE,
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 3;
  $pane->locks = array();
  $pane->uuid = 'e880d1c5-5691-46fb-aae1-bf75820cc608';
  $display->content['new-e880d1c5-5691-46fb-aae1-bf75820cc608'] = $pane;
  $display->panels['middle'][3] = 'new-e880d1c5-5691-46fb-aae1-bf75820cc608';
$display->hide_title = PANELS_TITLE_NONE;
$display->title_pane = '0';
$handler->conf['display'] = $display;
$page->default_handlers[$handler->name] = $handler;
