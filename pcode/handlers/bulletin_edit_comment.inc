<?php

$handler = new stdClass();
$handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
$handler->api_version = 1;
$handler->name = 'bulletin_edit_comment';
$handler->task = 'pm_existing_pages';
$handler->subtask = 'bulletin_edit_comment';
$handler->handler = 'panel_context';
$handler->weight = 0;
$handler->conf = array(
  'title' => 'Bulletin Comment Edit',
  'no_blocks' => 0,
  'pipeline' => 'standard',
  'body_classes_to_remove' => '',
  'body_classes_to_add' => '',
  'css_id' => '',
  'css' => '',
  'contexts' => array(),
  'relationships' => array(),
  'access' => array(
    'plugins' => array(
      0 => array(
        'name' => 'role',
        'settings' => array(
          'rids' => array(
            0 => 226993263,
          ),
        ),
        'context' => 'logged-in-user',
        'not' => FALSE,
      ),
    ),
    'logic' => 'and',
  ),
);
$display = new panels_display();
$display->layout = 'onecol';
$display->layout_settings = array();
$display->panel_settings = array(
  'style_settings' => array(
    'default' => NULL,
    'middle' => NULL,
  ),
);
$display->cache = array();
$display->title = '';
$display->uuid = '9443ef4d-e239-42fb-85f2-c97a7aba28a4';
$display->content = array();
$display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-f4d86bfc-17b9-48c9-aa30-26410d50ff51';
  $pane->panel = 'middle';
  $pane->type = 'custom';
  $pane->subtype = 'custom';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'admin_title' => '',
    'title' => '',
    'body' => '<div class="navigation-bar">	
	<div class="main-bar-items">
		<div class="comment-header">Edit Comment</div>
                 <div class="right-blox">
		</div>
	</div>
</div>',
    'format' => 'full_html',
    'substitute' => TRUE,
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = 'f4d86bfc-17b9-48c9-aa30-26410d50ff51';
  $display->content['new-f4d86bfc-17b9-48c9-aa30-26410d50ff51'] = $pane;
  $display->panels['middle'][0] = 'new-f4d86bfc-17b9-48c9-aa30-26410d50ff51';
  $pane = new stdClass();
  $pane->pid = 'new-70ac3a56-4c86-4a58-8731-e8b7d783a80c';
  $pane->panel = 'middle';
  $pane->type = 'pm_existing_pages';
  $pane->subtype = 'pm_existing_pages';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    0 => 'task_id',
    'task_id' => 'bulletin_edit_comment',
    'override_title' => 1,
    'override_title_text' => '',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array(
    'css_id' => '',
    'css_class' => 'body-cell',
  );
  $pane->extras = array();
  $pane->position = 1;
  $pane->locks = array();
  $pane->uuid = '70ac3a56-4c86-4a58-8731-e8b7d783a80c';
  $display->content['new-70ac3a56-4c86-4a58-8731-e8b7d783a80c'] = $pane;
  $display->panels['middle'][1] = 'new-70ac3a56-4c86-4a58-8731-e8b7d783a80c';
$display->hide_title = PANELS_TITLE_NONE;
$display->title_pane = '0';
$handler->conf['display'] = $display;
