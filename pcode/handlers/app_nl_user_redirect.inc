<?php

$handler = new stdClass();
$handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
$handler->api_version = 1;
$handler->name = 'app_nl_user_redirect';
$handler->task = 'pm_existing_pages';
$handler->subtask = 'user_redirect';
$handler->handler = 'http_response';
$handler->weight = 1;
$handler->conf = array(
  'title' => 'Mobile Redirect',
  'contexts' => array(),
  'relationships' => array(),
  'code' => '301',
  'destination' => 'citizen-reg',
  'access' => array(
    'plugins' => array(
      0 => array(
        'name' => 'theme',
        'settings' => array(
          'theme' => 'thebulletin',
        ),
        'not' => FALSE,
      ),
      1 => array(
        'name' => 'role',
        'settings' => array(
          'rids' => array(
            0 => 1,
          ),
        ),
        'context' => 'logged-in-user',
        'not' => FALSE,
      ),
    ),
    'logic' => 'and',
  ),
);
