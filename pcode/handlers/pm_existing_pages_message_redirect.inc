<?php

$handler = new stdClass();
$handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
$handler->api_version = 1;
$handler->name = 'pm_existing_pages_message_redirect';
$handler->task = 'pm_existing_pages';
$handler->subtask = 'message_redirect';
$handler->handler = 'http_response';
$handler->weight = 0;
$handler->conf = array(
  'title' => 'Redirect',
  'contexts' => array(),
  'relationships' => array(),
  'code' => '301',
  'destination' => 'messages/inbox',
);