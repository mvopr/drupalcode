<?php

$handler = new stdClass();
$handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
$handler->api_version = 1;
$handler->name = 'user_edit_citizen';
$handler->task = 'user_edit';
$handler->subtask = '';
$handler->handler = 'panel_context';
$handler->weight = 1;
$handler->conf = array(
 'title' => 'Citizen',
 'no_blocks' => 0,
 'pipeline' => 'standard',
 'body_classes_to_remove' => '',
 'body_classes_to_add' => '',
 'css_id' => '',
 'css' => '',
 'contexts' => array(),
 'relationships' => array(),
 'name' => 'reporter',
 'access' => array(
 'plugins' => array(
 0 => array(
 'name' => 'role',
 'settings' => array(
 'rids' => array(
 0 => 226993263,
 ),
 ),
 'context' => 'argument_user_edit_1',
 'not' => FALSE,
 ),
 ),
 'logic' => 'and',
 ),
);
$display = new panels_display();
$display->layout = 'onecol';
$display->layout_settings = array();
$display->panel_settings = array(
 'style_settings' => array(
 'default' => NULL,
 'middle' => NULL,
 ),
);
$display->cache = array();
$display->title = '';
$display->uuid = 'b5afa789-4f1a-49a1-9238-09488ebd4446';
$display->content = array();
$display->panels = array();
 $pane = new stdClass();
 $pane->pid = 'new-9cf229f8-cb4f-4d33-9f9c-d2e424916f30';
 $pane->panel = 'middle';
 $pane->type = 'custom';
 $pane->subtype = 'custom';
 $pane->shown = TRUE;
 $pane->access = array();
 $pane->configuration = array(
 'admin_title' => 'Menu Items',
 'title' => '',
 'body' => '<div class="navigation-bar main-bar-bg">	
	<div class="main-bar-items">
		<a href="?q=node/add/bulletin-source" id="post" target="white-frame"></a>
		<a href="/" class="bul-logo" id="bubble-top"></a>	
		<div class="right-blox">
			<ul>
				<li class="nav-icon"><a href="?q=dispatch" id="req-icon" target="darkbl-frame"></a></li>
				<li class="nav-icon"><div id="settings-icon"></div></li>
			</ul>
		</div>
	</div>
</div>

<div class="edit-menu">
<ul>
	<li ><a href="#" id="general-edit-tab">General</li>
	<li ><a href="#" id="account-edit-tab">Account</li>
</ul>
</div>',
 'format' => 'full_html',
 'substitute' => 1,
 );
 $pane->cache = array();
 $pane->style = array(
 'settings' => NULL,
 );
 $pane->css = array(
 'css_id' => '',
 'css_class' => '',
 );
 $pane->extras = array();
 $pane->position = 0;
 $pane->locks = array();
 $pane->uuid = '9cf229f8-cb4f-4d33-9f9c-d2e424916f30';
 $display->content['new-9cf229f8-cb4f-4d33-9f9c-d2e424916f30'] = $pane;
 $display->panels['middle'][0] = 'new-9cf229f8-cb4f-4d33-9f9c-d2e424916f30';
 $pane = new stdClass();
 $pane->pid = 'new-2cbca1ee-94f4-4420-8e50-af26de2d3a13';
 $pane->panel = 'middle';
 $pane->type = 'form';
 $pane->subtype = 'form';
 $pane->shown = TRUE;
 $pane->access = array();
 $pane->configuration = array(
 'context' => 'argument_user_edit_1',
 'override_title' => 1,
 'override_title_text' => '',
 'override_title_heading' => 'h2',
 );
 $pane->cache = array();
 $pane->style = array(
 'settings' => NULL,
 );
 $pane->css = array(
 'css_id' => '',
 'css_class' => 'profile-edit',
 );
 $pane->extras = array();
 $pane->position = 1;
 $pane->locks = array();
 $pane->uuid = '2cbca1ee-94f4-4420-8e50-af26de2d3a13';
 $display->content['new-2cbca1ee-94f4-4420-8e50-af26de2d3a13'] = $pane;
 $display->panels['middle'][1] = 'new-2cbca1ee-94f4-4420-8e50-af26de2d3a13';
 $pane = new stdClass();
 $pane->pid = 'new-8344c887-18d2-4c9a-8b5a-fc414e6e4464';
 $pane->panel = 'middle';
 $pane->type = 'custom';
 $pane->subtype = 'custom';
 $pane->shown = TRUE;
 $pane->access = array();
 $pane->configuration = array(
 'admin_title' => 'Nav Pan',
 'title' => '',
 'body' => '<div id="white-view" class="white-slide slide-pan">
<div id="load"></div>
<iframe name="white-frame" id="white-frame"></iframe>
</div>

<div id="second-nav" class="darkbl-slide slide-pan">
<div id="load"></div>
<iframe name="darkbl-frame" id="darkbl-frame"></iframe>
</div>',
 'format' => 'full_html',
 'substitute' => 1,
 );
 $pane->cache = array();
 $pane->style = array(
 'settings' => NULL,
 );
 $pane->css = array();
 $pane->extras = array();
 $pane->position = 2;
 $pane->locks = array();
 $pane->uuid = '8344c887-18d2-4c9a-8b5a-fc414e6e4464';
 $display->content['new-8344c887-18d2-4c9a-8b5a-fc414e6e4464'] = $pane;
 $display->panels['middle'][2] = 'new-8344c887-18d2-4c9a-8b5a-fc414e6e4464';
$display->hide_title = PANELS_TITLE_NONE;
$display->title_pane = '0';
$handler->conf['display'] = $display;
