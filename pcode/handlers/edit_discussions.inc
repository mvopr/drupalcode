<?php

$handler = new stdClass();
$handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
$handler->api_version = 1;
$handler->name = 'edit_discussions';
$handler->task = 'node_edit';
$handler->subtask = '';
$handler->handler = 'panel_context';
$handler->weight = 2;
$handler->conf = array(
  'title' => 'Discussion Edit',
  'no_blocks' => 0,
  'pipeline' => 'standard',
  'body_classes_to_remove' => '',
  'body_classes_to_add' => '',
  'css_id' => '',
  'css' => '',
  'contexts' => array(),
  'relationships' => array(),
  'access' => array(
    'plugins' => array(
      0 => array(
        'name' => 'role',
        'settings' => array(
          'rids' => array(
            0 => 84425795,
          ),
        ),
        'context' => 'logged-in-user',
        'not' => FALSE,
      ),
      1 => array(
        'name' => 'node_type',
        'settings' => array(
          'type' => array(
            'post' => 'post',
          ),
        ),
        'context' => 'argument_node_edit_1',
        'not' => FALSE,
      ),
    ),
    'logic' => 'and',
  ),
);
$display = new panels_display();
$display->layout = 'onecol';
$display->layout_settings = array();
$display->panel_settings = array(
  'style_settings' => array(
    'default' => NULL,
    'middle' => NULL,
  ),
);
$display->cache = array();
$display->title = '';
$display->uuid = '0c9a5df8-4625-43b6-85df-529ccbb32aa1';
$display->content = array();
$display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-131ed2a4-5c41-4c2e-bb5f-acb8042f6269';
  $pane->panel = 'middle';
  $pane->type = 'views';
  $pane->subtype = 'taxonomy_view';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_pager_settings' => 0,
    'use_pager' => 0,
    'nodes_per_page' => '0',
    'pager_id' => '0',
    'offset' => '0',
    'more_link' => 0,
    'feed_icons' => 0,
    'panel_args' => 0,
    'link_to_view' => 0,
    'args' => '',
    'url' => '',
    'display' => 'block',
    'override_title' => 1,
    'override_title_text' => '',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array(
    'css_id' => 'discussion-sidebar',
    'css_class' => 'second-nav',
  );
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '131ed2a4-5c41-4c2e-bb5f-acb8042f6269';
  $display->content['new-131ed2a4-5c41-4c2e-bb5f-acb8042f6269'] = $pane;
  $display->panels['middle'][0] = 'new-131ed2a4-5c41-4c2e-bb5f-acb8042f6269';
  $pane = new stdClass();
  $pane->pid = 'new-ac2d9a64-85c1-4177-bc85-7229c1ea10da';
  $pane->panel = 'middle';
  $pane->type = 'node_form_title';
  $pane->subtype = 'node_form_title';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'context' => 'argument_node_edit_1',
    'override_title' => 0,
    'override_title_text' => '',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array(
    'css_id' => '',
    'css_class' => 'single-col-top',
  );
  $pane->extras = array();
  $pane->position = 1;
  $pane->locks = array();
  $pane->uuid = 'ac2d9a64-85c1-4177-bc85-7229c1ea10da';
  $display->content['new-ac2d9a64-85c1-4177-bc85-7229c1ea10da'] = $pane;
  $display->panels['middle'][1] = 'new-ac2d9a64-85c1-4177-bc85-7229c1ea10da';
  $pane = new stdClass();
  $pane->pid = 'new-fb79a88e-ec08-4770-9055-d58d48ee7f66';
  $pane->panel = 'middle';
  $pane->type = 'entity_form_field';
  $pane->subtype = 'node:field_post_body';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'label' => '',
    'formatter' => '',
    'context' => 'argument_node_edit_1',
    'override_title' => 0,
    'override_title_text' => '',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array(
    'css_id' => '',
    'css_class' => 'single-col-mid',
  );
  $pane->extras = array();
  $pane->position = 2;
  $pane->locks = array();
  $pane->uuid = 'fb79a88e-ec08-4770-9055-d58d48ee7f66';
  $display->content['new-fb79a88e-ec08-4770-9055-d58d48ee7f66'] = $pane;
  $display->panels['middle'][2] = 'new-fb79a88e-ec08-4770-9055-d58d48ee7f66';
  $pane = new stdClass();
  $pane->pid = 'new-c6c958e0-869b-4a91-97fc-f7beca6b2a0d';
  $pane->panel = 'middle';
  $pane->type = 'entity_form_field';
  $pane->subtype = 'node:field_catigory';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'label' => '',
    'formatter' => '',
    'context' => 'argument_node_edit_1',
    'override_title' => 0,
    'override_title_text' => '',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array(
    'css_id' => '',
    'css_class' => 'single-col-mid',
  );
  $pane->extras = array();
  $pane->position = 3;
  $pane->locks = array();
  $pane->uuid = 'c6c958e0-869b-4a91-97fc-f7beca6b2a0d';
  $display->content['new-c6c958e0-869b-4a91-97fc-f7beca6b2a0d'] = $pane;
  $display->panels['middle'][3] = 'new-c6c958e0-869b-4a91-97fc-f7beca6b2a0d';
  $pane = new stdClass();
  $pane->pid = 'new-4ab06c1e-11b4-4611-b880-103bd7c72ec1';
  $pane->panel = 'middle';
  $pane->type = 'node_form_buttons';
  $pane->subtype = 'node_form_buttons';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'context' => 'argument_node_edit_1',
    'override_title' => 0,
    'override_title_text' => '',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array(
    'css_id' => '',
    'css_class' => 'single-col-mid',
  );
  $pane->extras = array();
  $pane->position = 4;
  $pane->locks = array();
  $pane->uuid = '4ab06c1e-11b4-4611-b880-103bd7c72ec1';
  $display->content['new-4ab06c1e-11b4-4611-b880-103bd7c72ec1'] = $pane;
  $display->panels['middle'][4] = 'new-4ab06c1e-11b4-4611-b880-103bd7c72ec1';
  $pane = new stdClass();
  $pane->pid = 'new-4172fb6e-e7da-4f90-a50d-6689427195ef';
  $pane->panel = 'middle';
  $pane->type = 'custom';
  $pane->subtype = 'custom';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'admin_title' => 'Placeholder JS',
    'title' => '',
    'body' => '<script type="text/javascript"> 
(function ($) {
$.ajax({
  url: "sites/all/themes/framework/inline.js",
  dataType: "script",
});
})(jQuery);
</script> ',
    'format' => 'full_html',
    'substitute' => 1,
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 5;
  $pane->locks = array();
  $pane->uuid = '4172fb6e-e7da-4f90-a50d-6689427195ef';
  $display->content['new-4172fb6e-e7da-4f90-a50d-6689427195ef'] = $pane;
  $display->panels['middle'][5] = 'new-4172fb6e-e7da-4f90-a50d-6689427195ef';
$display->hide_title = PANELS_TITLE_FIXED;
$display->title_pane = '0';
$handler->conf['display'] = $display;
