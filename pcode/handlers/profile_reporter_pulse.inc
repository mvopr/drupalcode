<?php

$handler = new stdClass();
$handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
$handler->api_version = 1;
$handler->name = 'profile_reporter_pulse';
$handler->task = 'user_view';
$handler->subtask = '';
$handler->handler = 'panel_context';
$handler->weight = -30;
$handler->conf = array(
  'title' => 'Reporter (Dispatch)',
  'no_blocks' => 0,
  'pipeline' => 'standard',
  'body_classes_to_remove' => '',
  'body_classes_to_add' => '',
  'css_id' => 'node-frame-twocol',
  'css' => '',
  'contexts' => array(),
  'relationships' => array(),
  'access' => array(
    'plugins' => array(
      0 => array(
        'name' => 'theme',
        'settings' => array(
          'theme' => 'framework',
        ),
        'not' => FALSE,
      ),
      1 => array(
        'name' => 'role',
        'settings' => array(
          'rids' => array(
            0 => 84425795,
          ),
        ),
        'context' => 'argument_entity_id:user_1',
        'not' => FALSE,
      ),
    ),
    'logic' => 'and',
  ),
);
$display = new panels_display();
$display->layout = 'onecol';
$display->layout_settings = array();
$display->panel_settings = array(
  'style_settings' => array(
    'default' => NULL,
    'middle' => NULL,
  ),
);
$display->cache = array();
$display->title = '';
$display->uuid = 'dd2f1a58-77a0-4484-80da-3432f39a3903';
$display->content = array();
$display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-9362b21f-6587-4591-b363-2b936c662fba';
  $pane->panel = 'middle';
  $pane->type = 'custom';
  $pane->subtype = 'custom';
  $pane->shown = TRUE;
  $pane->access = array(
    'plugins' => array(
      0 => array(
        'name' => 'compare_users',
        'settings' => array(
          'equality' => '1',
        ),
        'context' => array(
          0 => 'argument_entity_id:user_1',
          1 => 'logged-in-user',
        ),
        'not' => FALSE,
      ),
    ),
  );
  $pane->configuration = array(
    'admin_title' => 'URL Redirect',
    'title' => '',
    'body' => '<script>
window.location.replace("%user:edit-url");
</script>',
    'format' => 'full_html',
    'substitute' => 1,
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '9362b21f-6587-4591-b363-2b936c662fba';
  $display->content['new-9362b21f-6587-4591-b363-2b936c662fba'] = $pane;
  $display->panels['middle'][0] = 'new-9362b21f-6587-4591-b363-2b936c662fba';
  $pane = new stdClass();
  $pane->pid = 'new-65f3f377-96dd-41fd-b65c-4b091da84097';
  $pane->panel = 'middle';
  $pane->type = 'custom';
  $pane->subtype = 'custom';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'admin_title' => 'Close Btn',
    'title' => '',
    'body' => '<div class="close" id="node-frame-close">x</div>',
    'format' => 'full_html',
    'substitute' => 1,
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array(
    'css_id' => 'node-hide',
    'css_class' => '',
  );
  $pane->extras = array();
  $pane->position = 1;
  $pane->locks = array();
  $pane->uuid = '65f3f377-96dd-41fd-b65c-4b091da84097';
  $display->content['new-65f3f377-96dd-41fd-b65c-4b091da84097'] = $pane;
  $display->panels['middle'][1] = 'new-65f3f377-96dd-41fd-b65c-4b091da84097';
  $pane = new stdClass();
  $pane->pid = 'new-8341e7ac-2f11-40ef-8376-d5a75ea6d9a3';
  $pane->panel = 'middle';
  $pane->type = 'custom';
  $pane->subtype = 'custom';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'admin_title' => 'Map',
    'title' => '',
    'body' => '<iframe src="?q=map" id="iframe" name="iframe" class="map"></iframe>',
    'format' => 'full_html',
    'substitute' => 1,
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array(
    'css_id' => '',
    'css_class' => 'map-box',
  );
  $pane->extras = array();
  $pane->position = 2;
  $pane->locks = array();
  $pane->uuid = '8341e7ac-2f11-40ef-8376-d5a75ea6d9a3';
  $display->content['new-8341e7ac-2f11-40ef-8376-d5a75ea6d9a3'] = $pane;
  $display->panels['middle'][2] = 'new-8341e7ac-2f11-40ef-8376-d5a75ea6d9a3';
  $pane = new stdClass();
  $pane->pid = 'new-75187413-ce30-4d43-af83-7d943fa64eca';
  $pane->panel = 'middle';
  $pane->type = 'views';
  $pane->subtype = 'discussions';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_pager_settings' => 0,
    'use_pager' => 1,
    'nodes_per_page' => '25',
    'pager_id' => '0',
    'offset' => '0',
    'more_link' => 0,
    'feed_icons' => 0,
    'panel_args' => 0,
    'link_to_view' => 0,
    'args' => '',
    'url' => '',
    'display' => 'block_3',
    'context' => array(
      0 => '',
    ),
    'override_title' => 1,
    'override_title_text' => '',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array(
    'css_id' => 'node-hide',
    'css_class' => 'frame-right',
  );
  $pane->extras = array();
  $pane->position = 3;
  $pane->locks = array();
  $pane->uuid = '75187413-ce30-4d43-af83-7d943fa64eca';
  $display->content['new-75187413-ce30-4d43-af83-7d943fa64eca'] = $pane;
  $display->panels['middle'][3] = 'new-75187413-ce30-4d43-af83-7d943fa64eca';
  $pane = new stdClass();
  $pane->pid = 'new-8095daf2-c6d2-4c84-83b4-98d61bd5b97b';
  $pane->panel = 'middle';
  $pane->type = 'panels_mini';
  $pane->subtype = 'profile_citizen_pulse_side';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => 0,
    'override_title_text' => '',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array(
    'css_id' => 'node-hide',
    'css_class' => 'frame-left',
  );
  $pane->extras = array();
  $pane->position = 4;
  $pane->locks = array();
  $pane->uuid = '8095daf2-c6d2-4c84-83b4-98d61bd5b97b';
  $display->content['new-8095daf2-c6d2-4c84-83b4-98d61bd5b97b'] = $pane;
  $display->panels['middle'][4] = 'new-8095daf2-c6d2-4c84-83b4-98d61bd5b97b';
  $pane = new stdClass();
  $pane->pid = 'new-6b3e1180-4872-41b2-b1c9-6e1103ada029';
  $pane->panel = 'middle';
  $pane->type = 'custom';
  $pane->subtype = 'custom';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'admin_title' => 'Custom JS',
    'title' => '',
    'body' => '<script type="text/javascript"> 
(function ($) {
$.ajax({
  url: "sites/all/themes/framework/inline.js",
  dataType: "script",
});
})(jQuery);
</script> ',
    'format' => 'full_html',
    'substitute' => 1,
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 5;
  $pane->locks = array();
  $pane->uuid = '6b3e1180-4872-41b2-b1c9-6e1103ada029';
  $display->content['new-6b3e1180-4872-41b2-b1c9-6e1103ada029'] = $pane;
  $display->panels['middle'][5] = 'new-6b3e1180-4872-41b2-b1c9-6e1103ada029';
$display->hide_title = PANELS_TITLE_FIXED;
$display->title_pane = '0';
$handler->conf['display'] = $display;
