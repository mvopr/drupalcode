<?php

$handler = new stdClass();
$handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
$handler->api_version = 1;
$handler->name = 'profile_citizen_pulse';
$handler->task = 'user_view';
$handler->subtask = '';
$handler->handler = 'panel_context';
$handler->weight = 3;
$handler->conf = array(
  'title' => 'Citizen (Atlas Page)',
  'no_blocks' => 0,
  'pipeline' => 'standard',
  'body_classes_to_remove' => '',
  'body_classes_to_add' => 'citizen-profile',
  'css_id' => 'node-frame-twocol',
  'css' => '',
  'contexts' => array(),
  'relationships' => array(),
  'access' => array(
    'plugins' => array(
      0 => array(
        'name' => 'theme',
        'settings' => array(
          'theme' => 'framework',
        ),
        'not' => FALSE,
      ),
      1 => array(
        'name' => 'role',
        'settings' => array(
          'rids' => array(
            0 => 226993263,
          ),
        ),
        'context' => 'argument_entity_id:user_1',
        'not' => FALSE,
      ),
    ),
    'logic' => 'and',
  ),
);
$display = new panels_display();
$display->layout = 'onecol';
$display->layout_settings = array();
$display->panel_settings = array(
  'style_settings' => array(
    'default' => NULL,
    'middle' => NULL,
  ),
);
$display->cache = array();
$display->title = '';
$display->uuid = 'dd2f1a58-77a0-4484-80da-3432f39a3903';
$display->content = array();
$display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-b028d8f3-218b-43f9-92a5-25f3a3b92055';
  $pane->panel = 'middle';
  $pane->type = 'page_title';
  $pane->subtype = 'page_title';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'markup' => 'h1',
    'class' => '',
    'id' => '',
    'override_title' => 0,
    'override_title_text' => '',
    'override_title_heading' => 'h2',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array(
    'css_id' => '',
    'css_class' => 'frame-cell',
  );
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = 'b028d8f3-218b-43f9-92a5-25f3a3b92055';
  $display->content['new-b028d8f3-218b-43f9-92a5-25f3a3b92055'] = $pane;
  $display->panels['middle'][0] = 'new-b028d8f3-218b-43f9-92a5-25f3a3b92055';
  $pane = new stdClass();
  $pane->pid = 'new-7de89e32-5456-49a5-ab74-47711f796f5d';
  $pane->panel = 'middle';
  $pane->type = 'views';
  $pane->subtype = 'bulletin';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_pager_settings' => 0,
    'use_pager' => 0,
    'nodes_per_page' => '1',
    'pager_id' => '0',
    'offset' => '0',
    'more_link' => 0,
    'feed_icons' => 0,
    'panel_args' => 0,
    'link_to_view' => 0,
    'args' => '',
    'url' => '',
    'display' => 'block_2',
    'context' => array(
      0 => 'argument_entity_id:user_1.uid',
    ),
    'override_title' => 0,
    'override_title_text' => '',
    'override_title_heading' => 'h2',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array(
    'css_id' => '',
    'css_class' => 'frame-cell citizen-points',
  );
  $pane->extras = array();
  $pane->position = 1;
  $pane->locks = array();
  $pane->uuid = '7de89e32-5456-49a5-ab74-47711f796f5d';
  $display->content['new-7de89e32-5456-49a5-ab74-47711f796f5d'] = $pane;
  $display->panels['middle'][1] = 'new-7de89e32-5456-49a5-ab74-47711f796f5d';
  $pane = new stdClass();
  $pane->pid = 'new-6e7b1ea3-3a31-44e1-9a29-80c9aff7f934';
  $pane->panel = 'middle';
  $pane->type = 'views';
  $pane->subtype = 'bulletin';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_pager_settings' => 0,
    'use_pager' => 1,
    'nodes_per_page' => '15',
    'pager_id' => '0',
    'offset' => '0',
    'more_link' => 0,
    'feed_icons' => 0,
    'panel_args' => 0,
    'link_to_view' => 0,
    'args' => '',
    'url' => '',
    'display' => 'block_3',
    'context' => array(
      0 => '',
    ),
    'override_title' => 1,
    'override_title_text' => '',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array(
    'css_id' => '',
    'css_class' => 'frame-cell',
  );
  $pane->extras = array();
  $pane->position = 2;
  $pane->locks = array();
  $pane->uuid = '6e7b1ea3-3a31-44e1-9a29-80c9aff7f934';
  $display->content['new-6e7b1ea3-3a31-44e1-9a29-80c9aff7f934'] = $pane;
  $display->panels['middle'][2] = 'new-6e7b1ea3-3a31-44e1-9a29-80c9aff7f934';
$display->hide_title = PANELS_TITLE_FIXED;
$display->title_pane = '0';
$handler->conf['display'] = $display;
