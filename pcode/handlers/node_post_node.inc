<?php 

$handler = new stdClass();
$handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
$handler->api_version = 1;
$handler->name = 'node_post_node';
$handler->task = 'node_view';
$handler->subtask = '';
$handler->handler = 'panel_context';
$handler->weight = -28;
$handler->conf = array(
  'title' => 'Post Node',
  'no_blocks' => 0,
  'pipeline' => 'standard',
  'body_classes_to_remove' => '',
  'body_classes_to_add' => 'society-post',
  'css_id' => '',
  'css' => '',
  'contexts' => array(),
  'relationships' => array(),
  'access' => array(
    'plugins' => array(
      0 => array(
        'name' => 'node_type',
        'settings' => array(
          'type' => array(
            'post' => 'post',
          ),
        ),
        'context' => 'argument_entity_id:node_1',
        'not' => FALSE,
      ),
    ),
    'logic' => 'and',
  ),
);
$display = new panels_display();
$display->layout = 'onecol';
$display->layout_settings = array();
$display->panel_settings = array(
  'style_settings' => array(
    'default' => NULL,
    'center' => NULL,
    'middle' => NULL,
    'center_' => NULL,
    'right' => NULL,
    'top' => NULL,
    'title' => NULL,
  ),
);
$display->cache = array();
$display->title = '';
$display->uuid = 'dca5550e-2085-475d-9065-bd6228638a99';
$display->content = array();
$display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-d9573658-11e0-4011-85ea-96fe6fd0be85';
  $pane->panel = 'middle';
  $pane->type = 'views';
  $pane->subtype = 'taxonomy_view';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_pager_settings' => 0,
    'use_pager' => 0,
    'nodes_per_page' => '0',
    'pager_id' => '0',
    'offset' => '0',
    'more_link' => 0,
    'feed_icons' => 0,
    'panel_args' => 0,
    'link_to_view' => 0,
    'args' => '',
    'url' => '',
    'display' => 'block',
    'override_title' => 1,
    'override_title_text' => '',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array(
    'css_id' => 'discussion-sidebar',
    'css_class' => 'second-nav',
  );
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = 'd9573658-11e0-4011-85ea-96fe6fd0be85';
  $display->content['new-d9573658-11e0-4011-85ea-96fe6fd0be85'] = $pane;
  $display->panels['middle'][0] = 'new-d9573658-11e0-4011-85ea-96fe6fd0be85';
  $pane = new stdClass();
  $pane->pid = 'new-e30dc152-7a33-4afd-9f04-26a7feade344';
  $pane->panel = 'middle';
  $pane->type = 'panels_mini';
  $pane->subtype = 'node_post_sidebar';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'context' => array(
      0 => 'argument_entity_id:node_1',
    ),
    'override_title' => 0,
    'override_title_text' => '',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array(
    'css_id' => '',
    'css_class' => 'col-second',
  );
  $pane->extras = array();
  $pane->position = 1;
  $pane->locks = array();
  $pane->uuid = 'e30dc152-7a33-4afd-9f04-26a7feade344';
  $display->content['new-e30dc152-7a33-4afd-9f04-26a7feade344'] = $pane;
  $display->panels['middle'][1] = 'new-e30dc152-7a33-4afd-9f04-26a7feade344';
  $pane = new stdClass();
  $pane->pid = 'new-abb6f99b-9efe-4332-b65d-3e548c2705dc';
  $pane->panel = 'middle';
  $pane->type = 'panels_mini';
  $pane->subtype = 'node_post_body';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'context' => array(
      0 => 'argument_entity_id:node_1',
    ),
    'override_title' => 1,
    'override_title_text' => '',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array(
    'css_id' => '',
    'css_class' => 'col-main',
  );
  $pane->extras = array();
  $pane->position = 2;
  $pane->locks = array();
  $pane->uuid = 'abb6f99b-9efe-4332-b65d-3e548c2705dc';
  $display->content['new-abb6f99b-9efe-4332-b65d-3e548c2705dc'] = $pane;
  $display->panels['middle'][2] = 'new-abb6f99b-9efe-4332-b65d-3e548c2705dc';
$display->hide_title = PANELS_TITLE_FIXED;
$display->title_pane = '0';
$handler->conf['display'] = $display;
