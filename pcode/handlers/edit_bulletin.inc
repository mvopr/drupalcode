<?php

$handler = new stdClass();
$handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
$handler->api_version = 1;
$handler->name = 'edit_bulletin';
$handler->task = 'node_edit';
$handler->subtask = '';
$handler->handler = 'panel_context';
$handler->weight = 1;
$handler->conf = array(
  'title' => 'Bulletin Edit',
  'no_blocks' => 0,
  'pipeline' => 'standard',
  'body_classes_to_remove' => '',
  'body_classes_to_add' => '',
  'css_id' => '',
  'css' => '',
  'contexts' => array(),
  'relationships' => array(),
  'access' => array(
    'plugins' => array(
      0 => array(
        'name' => 'role',
        'settings' => array(
          'rids' => array(
            0 => 226993263,
          ),
        ),
        'context' => 'logged-in-user',
        'not' => FALSE,
      ),
      1 => array(
        'name' => 'theme',
        'settings' => array(
          'theme' => 'thebulletin',
        ),
        'not' => FALSE,
      ),
    ),
    'logic' => 'and',
  ),
);
$display = new panels_display();
$display->layout = 'onecol';
$display->layout_settings = array();
$display->panel_settings = array(
  'style_settings' => array(
    'default' => NULL,
    'middle' => NULL,
  ),
);
$display->cache = array();
$display->title = '';
$display->uuid = '9443ef4d-e239-42fb-85f2-c97a7aba28a4';
$display->content = array();
$display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-e1ed644c-3986-490a-9df1-1d48c7e0867f';
  $pane->panel = 'middle';
  $pane->type = 'custom';
  $pane->subtype = 'custom';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'admin_title' => 'Header',
    'title' => '',
    'body' => '<div class="navigation-bar nodebar-bg">	
	<div class="main-bar-items">
		<div class="right-blox">
			<ul>
				<li class="nav-icon"><div id="node-close" class="x-icon"></div></li>
			</ul>
		</div>
	</div>
</div>',
    'format' => 'full_html',
    'substitute' => 1,
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = 'e1ed644c-3986-490a-9df1-1d48c7e0867f';
  $display->content['new-e1ed644c-3986-490a-9df1-1d48c7e0867f'] = $pane;
  $display->panels['middle'][0] = 'new-e1ed644c-3986-490a-9df1-1d48c7e0867f';
  $pane = new stdClass();
  $pane->pid = 'new-8a475369-9c6d-4c7b-b222-c53b862eaff5';
  $pane->panel = 'middle';
  $pane->type = 'page_messages';
  $pane->subtype = 'page_messages';
  $pane->shown = FALSE;
  $pane->access = array();
  $pane->configuration = array();
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array(
    'css_id' => '',
    'css_class' => 'message',
  );
  $pane->extras = array();
  $pane->position = 1;
  $pane->locks = array();
  $pane->uuid = '8a475369-9c6d-4c7b-b222-c53b862eaff5';
  $display->content['new-8a475369-9c6d-4c7b-b222-c53b862eaff5'] = $pane;
  $display->panels['middle'][1] = 'new-8a475369-9c6d-4c7b-b222-c53b862eaff5';
  $pane = new stdClass();
  $pane->pid = 'new-a93545dc-d6e8-4d75-895e-e1ba424707ca';
  $pane->panel = 'middle';
  $pane->type = 'views';
  $pane->subtype = 'bulletin';
  $pane->shown = FALSE;
  $pane->access = array();
  $pane->configuration = array(
    'override_pager_settings' => 0,
    'use_pager' => 0,
    'nodes_per_page' => '1',
    'pager_id' => '0',
    'offset' => '0',
    'more_link' => 0,
    'feed_icons' => 0,
    'panel_args' => 0,
    'link_to_view' => 0,
    'args' => '',
    'url' => '',
    'display' => 'block_1',
    'context' => array(
      0 => '',
    ),
    'override_title' => 0,
    'override_title_text' => '',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array(
    'css_id' => '',
    'css_class' => 'stat-box',
  );
  $pane->extras = array();
  $pane->position = 2;
  $pane->locks = array();
  $pane->uuid = 'a93545dc-d6e8-4d75-895e-e1ba424707ca';
  $display->content['new-a93545dc-d6e8-4d75-895e-e1ba424707ca'] = $pane;
  $display->panels['middle'][2] = 'new-a93545dc-d6e8-4d75-895e-e1ba424707ca';
  $pane = new stdClass();
  $pane->pid = 'new-79b89104-550d-445c-9b67-95956d3595cc';
  $pane->panel = 'middle';
  $pane->type = 'custom';
  $pane->subtype = 'custom';
  $pane->shown = FALSE;
  $pane->access = array();
  $pane->configuration = array(
    'admin_title' => 'Cam Icon',
    'title' => '',
    'body' => '<div class="cam-icon"></div>
<script>
(function ($) {
    $(\'option[value="_none"]\').text(\'Select a beat\');
    $( ".image-widget-data" ).html( "<input type=\'file\' id=\'edit-field-bulletin-image-und-0-upload\' name=\'files[field_bulletin_image_und_0]\' size=\'22\' class=\'form-file\' accept=\'image/*\'><input type=\'submit\' id=\'edit-field-bulletin-image-und-0-upload-button\' name=\'field_bulletin_image_und_0_upload_button\' value=\'Upload\' class=\'form-submit\'>" );
})(jQuery);
</script>',
    'format' => 'full_html',
    'substitute' => TRUE,
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 3;
  $pane->locks = array();
  $pane->uuid = '79b89104-550d-445c-9b67-95956d3595cc';
  $display->content['new-79b89104-550d-445c-9b67-95956d3595cc'] = $pane;
  $display->panels['middle'][3] = 'new-79b89104-550d-445c-9b67-95956d3595cc';
  $pane = new stdClass();
  $pane->pid = 'new-b2f3060b-061c-46ea-84e9-216344022e34';
  $pane->panel = 'middle';
  $pane->type = 'entity_form_field';
  $pane->subtype = 'node:field_bulletin_body';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'label' => '',
    'formatter' => '',
    'context' => 'argument_node_edit_1',
    'override_title' => 1,
    'override_title_text' => '',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array(
    'css_id' => '',
    'css_class' => 'node-body edit',
  );
  $pane->extras = array();
  $pane->position = 4;
  $pane->locks = array();
  $pane->uuid = 'b2f3060b-061c-46ea-84e9-216344022e34';
  $display->content['new-b2f3060b-061c-46ea-84e9-216344022e34'] = $pane;
  $display->panels['middle'][4] = 'new-b2f3060b-061c-46ea-84e9-216344022e34';
  $pane = new stdClass();
  $pane->pid = 'new-382d57b3-a6e4-4b68-97f5-dc8a45014445';
  $pane->panel = 'middle';
  $pane->type = 'entity_form_field';
  $pane->subtype = 'node:field_bulletin_image';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'label' => '',
    'formatter' => '',
    'context' => 'argument_node_edit_1',
    'override_title' => 1,
    'override_title_text' => '',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array(
    'css_id' => '',
    'css_class' => 'node-body node-body-mid',
  );
  $pane->extras = array();
  $pane->position = 5;
  $pane->locks = array();
  $pane->uuid = '382d57b3-a6e4-4b68-97f5-dc8a45014445';
  $display->content['new-382d57b3-a6e4-4b68-97f5-dc8a45014445'] = $pane;
  $display->panels['middle'][5] = 'new-382d57b3-a6e4-4b68-97f5-dc8a45014445';
  $pane = new stdClass();
  $pane->pid = 'new-affb8967-a1fb-4d3e-bf62-b7e6c01651d3';
  $pane->panel = 'middle';
  $pane->type = 'node_form_buttons';
  $pane->subtype = 'node_form_buttons';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'context' => 'argument_node_edit_1',
    'override_title' => 1,
    'override_title_text' => '',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array(
    'css_id' => '',
    'css_class' => 'node-body',
  );
  $pane->extras = array();
  $pane->position = 6;
  $pane->locks = array();
  $pane->uuid = 'affb8967-a1fb-4d3e-bf62-b7e6c01651d3';
  $display->content['new-affb8967-a1fb-4d3e-bf62-b7e6c01651d3'] = $pane;
  $display->panels['middle'][6] = 'new-affb8967-a1fb-4d3e-bf62-b7e6c01651d3';
  $pane = new stdClass();
  $pane->pid = 'new-fa7a2cb0-5510-4444-81eb-c98bc2951fd9';
  $pane->panel = 'middle';
  $pane->type = 'custom';
  $pane->subtype = 'custom';
  $pane->shown = FALSE;
  $pane->access = array();
  $pane->configuration = array(
    'admin_title' => 'JS Items',
    'title' => '',
    'body' => '<style>
	.form-radio {
	display: none;
}
</style>

<script type="text/javascript"> 
(function ($) {
$("#edit-title-field-und-0-value").focus(function(){
	if ($("#content").scrollTop() < 100) {
  	$("#content").animate({scrollTop: \'+=320px\'});
}
});

$("#edit-field-bulletin-body-und-0-value").focus(function(){
	if ($("#content").scrollTop() < 100) {
  	$("#content").animate({scrollTop: \'+=320px\'});
}
});

$("#edit-field-bulletin-body-und-0-value").focus(function(){
	$("#content").css("margin-top","-90px");
});

$("#edit-field-bulletin-body-und-0-value").blur(function(){
		$("#content").css("margin-top","0px");
});

$( document ).ready(function() {
	$(\'head\').append(\'<style id="additionalStyles">.image-widget-data:before{content:\\\'Upload Photo\\\';}</style>\');
});
$("#edit-field-bulletin-image-und-0-upload").change( function() {
	$(\'head\').append(\'<style id="additionalStyles">.image-widget-data:before{content:\\\'Photo Uploaded\\\';}</style>\');
});

if ($(\'.image-preview\').length > 0) {
    $("#edit-field-bulletin-image-und-0-ajax-wrapper").html("<input type=\'submit\' id=\'edit-field-bulletin-image-und-0-remove-button\' name=\'field_bulletin_image_und_0_remove_button\' value=\'Remove Image\' class=\'form-submit\'>");
};

jQuery(\'#edit-field-bulletin-image-und-0-upload\').attr(\'accept\', \'image/*\');

$(".cam-icon, .pin-icon").click(function(){
    $("#content").animate({scrollTop: \'+=320px\'});
});

$( "#load-graphic, #back-btn, .form-submit" ).click(function() {
  $( ".loading" ).css("display", "block");
  $( ".panel-col" ).css("display", "none");  
});

})(jQuery);
</script> ',
    'format' => 'full_html',
    'substitute' => TRUE,
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 7;
  $pane->locks = array();
  $pane->uuid = 'fa7a2cb0-5510-4444-81eb-c98bc2951fd9';
  $display->content['new-fa7a2cb0-5510-4444-81eb-c98bc2951fd9'] = $pane;
  $display->panels['middle'][7] = 'new-fa7a2cb0-5510-4444-81eb-c98bc2951fd9';
$display->hide_title = PANELS_TITLE_NONE;
$display->title_pane = 'new-a93545dc-d6e8-4d75-895e-e1ba424707ca';
$handler->conf['display'] = $display;
