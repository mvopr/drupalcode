<?php

$handler = new stdClass();
$handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
$handler->api_version = 1;
$handler->name = 'node_bulletin_report-ciz';
$handler->task = 'node_view';
$handler->subtask = '';
$handler->handler = 'panel_context';
$handler->weight = 6;
$handler->conf = array(
  'title' => 'Bulletin Report (citizen view)',
  'no_blocks' => 0,
  'pipeline' => 'standard',
  'body_classes_to_remove' => '',
  'body_classes_to_add' => '',
  'css_id' => '',
  'css' => '',
  'contexts' => array(),
  'relationships' => array(),
  'access' => array(
    'plugins' => array(
      0 => array(
        'name' => 'role',
        'settings' => array(
          'rids' => array(
            0 => 226993263,
          ),
        ),
        'context' => 'logged-in-user',
        'not' => FALSE,
      ),
    ),
    'logic' => 'and',
  ),
);
$display = new panels_display();
$display->layout = 'onecol';
$display->layout_settings = array();
$display->panel_settings = array(
  'style_settings' => array(
    'default' => NULL,
    'middle' => NULL,
  ),
);
$display->cache = array();
$display->title = '';
$display->uuid = '1b06ad32-04ca-4354-ace6-058b7da190fb';
$display->content = array();
$display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-7ffa2157-bf13-43ee-8fb0-0cee1acb5355';
  $pane->panel = 'middle';
  $pane->type = 'custom';
  $pane->subtype = 'custom';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'admin_title' => 'Nav Main',
    'title' => '',
    'body' => '<div class="navigation-bar">
	<div class="main-bar-items">
                 <div class="right-blox">
			<ul>
				<li class="nav-icon"><a href="opr-share://" id="share-icon"></a></li>
				<li class="nav-icon"><a href="%node:edit-url" id="edit-icon"></a></li>
				<li class="nav-icon"><div id="node-close" class="x-icon"></div></li>
			</ul>
		</div>
	</div>
</div>',
    'format' => 'full_html',
    'substitute' => 1,
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '7ffa2157-bf13-43ee-8fb0-0cee1acb5355';
  $display->content['new-7ffa2157-bf13-43ee-8fb0-0cee1acb5355'] = $pane;
  $display->panels['middle'][0] = 'new-7ffa2157-bf13-43ee-8fb0-0cee1acb5355';
  $pane = new stdClass();
  $pane->pid = 'new-2f513ff1-13bf-4f23-9025-abed3f90969e';
  $pane->panel = 'middle';
  $pane->type = 'views';
  $pane->subtype = 'bulletin';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_pager_settings' => 0,
    'use_pager' => 0,
    'nodes_per_page' => '1',
    'pager_id' => '0',
    'offset' => '0',
    'more_link' => 0,
    'feed_icons' => 0,
    'panel_args' => 0,
    'link_to_view' => 0,
    'args' => '',
    'url' => '',
    'display' => 'block_9',
    'context' => array(
      0 => '',
    ),
    'override_title' => 1,
    'override_title_text' => '',
    'override_title_heading' => 'h2',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array(
    'css_id' => '',
    'css_class' => 'body-cell',
  );
  $pane->extras = array();
  $pane->position = 1;
  $pane->locks = array();
  $pane->uuid = '2f513ff1-13bf-4f23-9025-abed3f90969e';
  $display->content['new-2f513ff1-13bf-4f23-9025-abed3f90969e'] = $pane;
  $display->panels['middle'][1] = 'new-2f513ff1-13bf-4f23-9025-abed3f90969e';
  $pane = new stdClass();
  $pane->pid = 'new-eb8cedf2-13a4-4c54-82d3-3613e655cacd';
  $pane->panel = 'middle';
  $pane->type = 'views';
  $pane->subtype = 'bulletin';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_pager_settings' => 0,
    'use_pager' => 0,
    'nodes_per_page' => '1',
    'pager_id' => '0',
    'offset' => '0',
    'more_link' => 0,
    'feed_icons' => 0,
    'panel_args' => 0,
    'link_to_view' => 0,
    'args' => '',
    'url' => '',
    'display' => 'block_7',
    'context' => array(
      0 => '',
    ),
    'override_title' => 0,
    'override_title_text' => '',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array(
    'css_id' => '',
    'css_class' => 'body-cell',
  );
  $pane->extras = array();
  $pane->position = 2;
  $pane->locks = array();
  $pane->uuid = 'eb8cedf2-13a4-4c54-82d3-3613e655cacd';
  $display->content['new-eb8cedf2-13a4-4c54-82d3-3613e655cacd'] = $pane;
  $display->panels['middle'][2] = 'new-eb8cedf2-13a4-4c54-82d3-3613e655cacd';
  $pane = new stdClass();
  $pane->pid = 'new-e58e95a5-a8cf-4ccb-abb6-8b17a131468c';
  $pane->panel = 'middle';
  $pane->type = 'entity_field';
  $pane->subtype = 'node:field_bulletin_body';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'label' => 'title',
    'formatter' => 'text_plain',
    'delta_limit' => 0,
    'delta_offset' => '0',
    'delta_reversed' => FALSE,
    'formatter_settings' => array(
      'trim_length' => '0',
      'ellipsis' => 1,
      'word_boundary' => 1,
      'token_replace' => 0,
      'filter' => 'input',
      'format' => 'plain_text',
      'allowed_html' => array(
        0 => 'a',
        1 => 'b',
        2 => 'br',
        3 => 'dd',
        4 => 'dl',
        5 => 'dt',
        6 => 'em',
        7 => 'i',
        8 => 'li',
        9 => 'ol',
        10 => 'p',
        11 => 'strong',
        12 => 'u',
        13 => 'ul',
      ),
      'autop' => 0,
      'use_summary' => 0,
    ),
    'context' => 'argument_entity_id:node_1',
    'override_title' => 1,
    'override_title_text' => '',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array(
    'css_id' => '',
    'css_class' => 'body-cell',
  );
  $pane->extras = array();
  $pane->position = 3;
  $pane->locks = array();
  $pane->uuid = 'e58e95a5-a8cf-4ccb-abb6-8b17a131468c';
  $display->content['new-e58e95a5-a8cf-4ccb-abb6-8b17a131468c'] = $pane;
  $display->panels['middle'][3] = 'new-e58e95a5-a8cf-4ccb-abb6-8b17a131468c';
  $pane = new stdClass();
  $pane->pid = 'new-36ae8b78-25bf-475e-a3e8-688e66e4ba1f';
  $pane->panel = 'middle';
  $pane->type = 'views';
  $pane->subtype = 'comments';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_pager_settings' => 0,
    'use_pager' => 1,
    'nodes_per_page' => '15',
    'pager_id' => '0',
    'offset' => '0',
    'more_link' => 0,
    'feed_icons' => 0,
    'panel_args' => 0,
    'link_to_view' => 0,
    'args' => '',
    'url' => '',
    'display' => 'block_3',
    'context' => array(
      0 => '',
    ),
    'override_title' => 1,
    'override_title_text' => '',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array(
    'css_id' => 'comments-com',
    'css_class' => 'body-cell',
  );
  $pane->extras = array();
  $pane->position = 4;
  $pane->locks = array();
  $pane->uuid = '36ae8b78-25bf-475e-a3e8-688e66e4ba1f';
  $display->content['new-36ae8b78-25bf-475e-a3e8-688e66e4ba1f'] = $pane;
  $display->panels['middle'][4] = 'new-36ae8b78-25bf-475e-a3e8-688e66e4ba1f';
  $pane = new stdClass();
  $pane->pid = 'new-e6ee244f-c117-4bd7-b66b-353822c4613e';
  $pane->panel = 'middle';
  $pane->type = 'custom';
  $pane->subtype = 'custom';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'admin_title' => 'Post Response',
    'title' => '',
    'body' => '<div  class="btn" id="com-btn">Comment</div>
<div id="poof"></div>',
    'format' => 'full_html',
    'substitute' => 0,
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array(
    'css_id' => 'btn-shell',
    'css_class' => 'body-cell',
  );
  $pane->extras = array();
  $pane->position = 5;
  $pane->locks = array();
  $pane->uuid = 'e6ee244f-c117-4bd7-b66b-353822c4613e';
  $display->content['new-e6ee244f-c117-4bd7-b66b-353822c4613e'] = $pane;
  $display->panels['middle'][5] = 'new-e6ee244f-c117-4bd7-b66b-353822c4613e';
  $pane = new stdClass();
  $pane->pid = 'new-d3f36ef5-fb2f-42e9-a6cd-b60f71bfc372';
  $pane->panel = 'middle';
  $pane->type = 'panels_mini';
  $pane->subtype = 'bulletin_comment_form';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'context' => array(
      0 => 'argument_entity_id:node_1',
    ),
    'override_title' => 1,
    'override_title_text' => '',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array(
    'css_id' => 'comments-wrap',
    'css_class' => 'comments-form',
  );
  $pane->extras = array();
  $pane->position = 6;
  $pane->locks = array();
  $pane->uuid = 'd3f36ef5-fb2f-42e9-a6cd-b60f71bfc372';
  $display->content['new-d3f36ef5-fb2f-42e9-a6cd-b60f71bfc372'] = $pane;
  $display->panels['middle'][6] = 'new-d3f36ef5-fb2f-42e9-a6cd-b60f71bfc372';
$display->hide_title = PANELS_TITLE_NONE;
$display->title_pane = '0';
$handler->conf['display'] = $display;
