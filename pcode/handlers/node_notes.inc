<?php

$handler = new stdClass();
$handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
$handler->api_version = 1;
$handler->name = 'node_notes';
$handler->task = 'node_view';
$handler->subtask = '';
$handler->handler = 'panel_context';
$handler->weight = 8;
$handler->conf = array(
  'title' => 'Notes',
  'no_blocks' => 0,
  'pipeline' => 'standard',
  'body_classes_to_remove' => '',
  'body_classes_to_add' => '',
  'css_id' => '',
  'css' => '',
  'contexts' => array(),
  'relationships' => array(),
  'access' => array(
    'plugins' => array(
      0 => array(
        'name' => 'role',
        'settings' => array(
          'rids' => array(
            0 => 225332102,
            1 => 172216448,
            2 => 185875538,
            3 => 40207975,
            4 => 203373462,
          ),
        ),
        'context' => 'logged-in-user',
        'not' => FALSE,
      ),
      1 => array(
        'name' => 'node_type',
        'settings' => array(
          'type' => array(
            'notes' => 'notes',
          ),
        ),
        'context' => 'argument_entity_id:node_1',
        'not' => FALSE,
      ),
    ),
    'logic' => 'and',
  ),
);
$display = new panels_display();
$display->layout = 'onecol';
$display->layout_settings = array();
$display->panel_settings = array(
  'style_settings' => array(
    'default' => NULL,
    'middle' => NULL,
  ),
);
$display->cache = array();
$display->title = '%node:title';
$display->uuid = '0c9a5df8-4625-43b6-85df-529ccbb32aa1';
$display->content = array();
$display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-ebcb4d88-a6d0-432b-89df-0335f8e8010e';
  $pane->panel = 'middle';
  $pane->type = 'views';
  $pane->subtype = 'notes';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_pager_settings' => 0,
    'use_pager' => 1,
    'nodes_per_page' => '10',
    'pager_id' => '0',
    'offset' => '0',
    'more_link' => 0,
    'feed_icons' => 0,
    'panel_args' => 0,
    'link_to_view' => 0,
    'args' => '',
    'url' => '',
    'display' => 'block_1',
    'override_title' => 1,
    'override_title_text' => '',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array(
    'css_id' => 'notes-sidebar',
    'css_class' => 'second-nav',
  );
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = 'ebcb4d88-a6d0-432b-89df-0335f8e8010e';
  $display->content['new-ebcb4d88-a6d0-432b-89df-0335f8e8010e'] = $pane;
  $display->panels['middle'][0] = 'new-ebcb4d88-a6d0-432b-89df-0335f8e8010e';
  $pane = new stdClass();
  $pane->pid = 'new-0e5ca680-e541-4ca7-bf63-edcbafc2b507';
  $pane->panel = 'middle';
  $pane->type = 'node_title';
  $pane->subtype = 'node_title';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'link' => 0,
    'markup' => 'h1',
    'id' => '',
    'class' => '',
    'context' => 'argument_entity_id:node_1',
    'override_title' => 1,
    'override_title_text' => '',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array(
    'css_id' => '',
    'css_class' => 'single-col-body',
  );
  $pane->extras = array();
  $pane->position = 1;
  $pane->locks = array();
  $pane->uuid = '0e5ca680-e541-4ca7-bf63-edcbafc2b507';
  $display->content['new-0e5ca680-e541-4ca7-bf63-edcbafc2b507'] = $pane;
  $display->panels['middle'][1] = 'new-0e5ca680-e541-4ca7-bf63-edcbafc2b507';
  $pane = new stdClass();
  $pane->pid = 'new-1cddf191-1bb5-4dde-b480-6b8a8167d678';
  $pane->panel = 'middle';
  $pane->type = 'views';
  $pane->subtype = 'notes';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_pager_settings' => 0,
    'use_pager' => 1,
    'nodes_per_page' => '10',
    'pager_id' => '0',
    'offset' => '0',
    'more_link' => 0,
    'feed_icons' => 0,
    'panel_args' => 0,
    'link_to_view' => 0,
    'args' => '',
    'url' => '',
    'display' => 'block_2',
    'context' => array(
      0 => '',
    ),
    'override_title' => 1,
    'override_title_text' => '',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array(
    'css_id' => '',
    'css_class' => 'single-col-mid',
  );
  $pane->extras = array();
  $pane->position = 2;
  $pane->locks = array();
  $pane->uuid = '1cddf191-1bb5-4dde-b480-6b8a8167d678';
  $display->content['new-1cddf191-1bb5-4dde-b480-6b8a8167d678'] = $pane;
  $display->panels['middle'][2] = 'new-1cddf191-1bb5-4dde-b480-6b8a8167d678';
  $pane = new stdClass();
  $pane->pid = 'new-e9b5ee07-d922-4d26-9eec-2745cce0cb63';
  $pane->panel = 'middle';
  $pane->type = 'entity_view';
  $pane->subtype = 'node';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'view_mode' => 'full',
    'context' => 'argument_entity_id:node_1',
    'override_title' => 0,
    'override_title_text' => '',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array(
    'css_id' => '',
    'css_class' => 'single-col-body',
  );
  $pane->extras = array();
  $pane->position = 3;
  $pane->locks = array();
  $pane->uuid = 'e9b5ee07-d922-4d26-9eec-2745cce0cb63';
  $display->content['new-e9b5ee07-d922-4d26-9eec-2745cce0cb63'] = $pane;
  $display->panels['middle'][3] = 'new-e9b5ee07-d922-4d26-9eec-2745cce0cb63';
  $pane = new stdClass();
  $pane->pid = 'new-8a771851-a10a-46a1-b0c3-d023732c279a';
  $pane->panel = 'middle';
  $pane->type = 'custom';
  $pane->subtype = 'custom';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'admin_title' => 'edit url',
    'title' => '',
    'body' => '<a href="%node:edit-url">edit</a>',
    'format' => 'filtered_html',
    'substitute' => 1,
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array(
    'css_id' => '',
    'css_class' => 'single-col-body',
  );
  $pane->extras = array();
  $pane->position = 4;
  $pane->locks = array();
  $pane->uuid = '8a771851-a10a-46a1-b0c3-d023732c279a';
  $display->content['new-8a771851-a10a-46a1-b0c3-d023732c279a'] = $pane;
  $display->panels['middle'][4] = 'new-8a771851-a10a-46a1-b0c3-d023732c279a';
$display->hide_title = PANELS_TITLE_FIXED;
$display->title_pane = '0';
$handler->conf['display'] = $display;
