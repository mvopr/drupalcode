<?php

$handler = new stdClass();
$handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
$handler->api_version = 1;
$handler->name = 'pm_existing_pages_password_reset';
$handler->task = 'pm_existing_pages';
$handler->subtask = 'password_reset';
$handler->handler = 'panel_context';
$handler->weight = 0;
$handler->conf = array(
  'title' => 'Panel',
  'no_blocks' => 0,
  'pipeline' => 'standard',
  'body_classes_to_remove' => '',
  'body_classes_to_add' => 'corebg',
  'css_id' => 'pass',
  'css' => '',
  'contexts' => array(),
  'relationships' => array(),
);
$display = new panels_display();
$display->layout = 'onecol';
$display->layout_settings = array();
$display->panel_settings = array(
  'style_settings' => array(
    'default' => NULL,
    'center' => NULL,
    'middle' => NULL,
  ),
);
$display->cache = array();
$display->title = 'Password Reset';
$display->uuid = 'd8dc8347-b3df-409b-8b3c-e6ef34bfd10d';
$display->content = array();
$display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-e0475a97-54ff-47cd-8e75-8cee9d5e9eff';
  $pane->panel = 'middle';
  $pane->type = 'page_messages';
  $pane->subtype = 'page_messages';
  $pane->shown = TRUE;
  $pane->access = array(
    'plugins' => array(),
  );
  $pane->configuration = array();
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array(
    'css_id' => '',
    'css_class' => 'node-body',
  );
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = 'e0475a97-54ff-47cd-8e75-8cee9d5e9eff';
  $display->content['new-e0475a97-54ff-47cd-8e75-8cee9d5e9eff'] = $pane;
  $display->panels['middle'][0] = 'new-e0475a97-54ff-47cd-8e75-8cee9d5e9eff';
  $pane = new stdClass();
  $pane->pid = 'new-226357a3-c6f5-465d-a454-243ffde17af1';
  $pane->panel = 'middle';
  $pane->type = 'pm_existing_pages';
  $pane->subtype = 'pm_existing_pages';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    0 => 'task_id',
    'task_id' => 'password_reset',
    'override_title' => 1,
    'override_title_text' => '',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array(
    'css_id' => '',
    'css_class' => 'app-login-box',
  );
  $pane->extras = array();
  $pane->position = 1;
  $pane->locks = array();
  $pane->uuid = '226357a3-c6f5-465d-a454-243ffde17af1';
  $display->content['new-226357a3-c6f5-465d-a454-243ffde17af1'] = $pane;
  $display->panels['middle'][1] = 'new-226357a3-c6f5-465d-a454-243ffde17af1';
$display->hide_title = PANELS_TITLE_NONE;
$display->title_pane = '0';
$handler->conf['display'] = $display;
