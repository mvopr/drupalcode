<?php

$handler = new stdClass();
$handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
$handler->api_version = 1;
$handler->name = 'group_manager';
$handler->task = 'group_view';
$handler->subtask = '';
$handler->handler = 'panel_context';
$handler->weight = 0;
$handler->conf = array(
  'title' => 'Group Mangager',
  'no_blocks' => 0,
  'pipeline' => 'standard',
  'body_classes_to_remove' => '',
  'body_classes_to_add' => 'group-shell',
  'css_id' => '',
  'css' => '',
  'contexts' => array(),
  'relationships' => array(),
  'name' => 'group_mangager',
  'access' => array(
    'plugins' => array(
      0 => array(
        'name' => 'role',
        'settings' => array(
          'rids' => array(
            0 => 143382413,
          ),
        ),
        'context' => 'logged-in-user',
        'not' => FALSE,
      ),
    ),
    'logic' => 'and',
  ),
);
$display = new panels_display();
$display->layout = 'onecol';
$display->layout_settings = array();
$display->panel_settings = array(
  'style_settings' => array(
    'default' => NULL,
    'middle' => NULL,
  ),
);
$display->cache = array();
$display->title = '';
$display->uuid = '86dadcf4-3522-460e-bc80-1bddd9f2ed6a';
$display->content = array();
$display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-c8d88ce9-8cc1-405a-8178-20e508876529';
  $pane->panel = 'middle';
  $pane->type = 'custom';
  $pane->subtype = 'custom';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'admin_title' => 'iframe',
    'title' => '',
    'body' => '<ul class="member-mng-nav">
<li><a href="?q=group/%group:gid/member" target="member-frame">Manage Users</a></li>
<li><a href="?q=group/%group:gid/invite/mail" target="member-frame">Add User</a></li>
<li><a href="?q=group/%group:gid/invite/mail-list" target="member-frame">Pending User</a></li>
</ul>

<iframe src="?q=group/%group:gid/member" name="member-frame" class="member-iframe" width="100%" height="100%" frameborder="0"></div>',
    'format' => 'full_html',
    'substitute' => 1,
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = 'c8d88ce9-8cc1-405a-8178-20e508876529';
  $display->content['new-c8d88ce9-8cc1-405a-8178-20e508876529'] = $pane;
  $display->panels['middle'][0] = 'new-c8d88ce9-8cc1-405a-8178-20e508876529';
$display->hide_title = PANELS_TITLE_FIXED;
$display->title_pane = 'new-c8d88ce9-8cc1-405a-8178-20e508876529';
$handler->conf['display'] = $display;
