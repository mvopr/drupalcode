<?php

$handler = new stdClass();
$handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
$handler->api_version = 1;
$handler->name = 'dispatch-tax';
$handler->task = 'term_view';
$handler->subtask = '';
$handler->handler = 'panel_context';
$handler->weight = 1;
$handler->conf = array(
  'title' => 'Dispatch Items',
  'no_blocks' => 0,
  'pipeline' => 'standard',
  'body_classes_to_remove' => '',
  'body_classes_to_add' => 'dispatch-tax',
  'css_id' => '',
  'css' => '',
  'contexts' => array(),
  'relationships' => array(),
  'name' => 'dispatch_items',
  'access' => array(
    'plugins' => array(
      0 => array(
        'name' => 'term_vocabulary',
        'settings' => array(
          'machine_name' => array(
            'dispatches' => 'dispatches',
          ),
        ),
        'context' => 'argument_term_1',
        'not' => FALSE,
      ),
    ),
    'logic' => 'and',
  ),
);
$display = new panels_display();
$display->layout = 'onecol';
$display->layout_settings = array();
$display->panel_settings = array(
  'style_settings' => array(
    'default' => NULL,
    'middle' => NULL,
  ),
);
$display->cache = array();
$display->title = '';
$display->uuid = 'e436cabb-e8bd-46c0-9aae-d7a3fc97d5e5';
$display->content = array();
$display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-a9023f3c-9f00-415e-be85-999bc6ad4cd9';
  $pane->panel = 'middle';
  $pane->type = 'term_name';
  $pane->subtype = 'term_name';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'link' => 0,
    'markup' => 'none',
    'id' => '',
    'class' => '',
    'context' => 'argument_term_1',
    'override_title' => 1,
    'override_title_text' => '',
    'override_title_heading' => 'h2',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = 'a9023f3c-9f00-415e-be85-999bc6ad4cd9';
  $display->content['new-a9023f3c-9f00-415e-be85-999bc6ad4cd9'] = $pane;
  $display->panels['middle'][0] = 'new-a9023f3c-9f00-415e-be85-999bc6ad4cd9';
  $pane = new stdClass();
  $pane->pid = 'new-d9e11b01-83c8-405b-95b1-afc2f5b4b141';
  $pane->panel = 'middle';
  $pane->type = 'entity_field_extra';
  $pane->subtype = 'taxonomy_term:description';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'view_mode' => 'full',
    'context' => 'argument_term_1',
    'override_title' => 0,
    'override_title_text' => '',
    'override_title_heading' => 'h2',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 1;
  $pane->locks = array();
  $pane->uuid = 'd9e11b01-83c8-405b-95b1-afc2f5b4b141';
  $display->content['new-d9e11b01-83c8-405b-95b1-afc2f5b4b141'] = $pane;
  $display->panels['middle'][1] = 'new-d9e11b01-83c8-405b-95b1-afc2f5b4b141';
  $pane = new stdClass();
  $pane->pid = 'new-2b521f59-dbb1-4dce-94e8-7ab9db083c27';
  $pane->panel = 'middle';
  $pane->type = 'views';
  $pane->subtype = 'dispatch';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_pager_settings' => 0,
    'use_pager' => 1,
    'nodes_per_page' => '15',
    'pager_id' => '0',
    'offset' => '0',
    'more_link' => 0,
    'feed_icons' => 0,
    'panel_args' => 0,
    'link_to_view' => 0,
    'args' => '',
    'url' => '',
    'display' => 'block_2',
    'override_title' => 1,
    'override_title_text' => '',
    'override_title_heading' => 'h2',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 2;
  $pane->locks = array();
  $pane->uuid = '2b521f59-dbb1-4dce-94e8-7ab9db083c27';
  $display->content['new-2b521f59-dbb1-4dce-94e8-7ab9db083c27'] = $pane;
  $display->panels['middle'][2] = 'new-2b521f59-dbb1-4dce-94e8-7ab9db083c27';
  $pane = new stdClass();
  $pane->pid = 'new-2293df74-ff7f-43b9-80b1-61e89881e9c9';
  $pane->panel = 'middle';
  $pane->type = 'flag_link';
  $pane->subtype = 'taxonomy_term';
  $pane->shown = TRUE;
  $pane->access = array(
    'plugins' => array(
      0 => array(
        'name' => 'role',
        'settings' => array(
          'rids' => array(
            0 => 226993263,
          ),
        ),
        'context' => 'logged-in-user',
        'not' => FALSE,
      ),
    ),
  );
  $pane->configuration = array(
    'flag_name' => 'hashtag_follow',
    'context' => 'argument_term_1',
    'override_title' => 1,
    'override_title_text' => '',
    'override_title_heading' => 'h2',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 3;
  $pane->locks = array();
  $pane->uuid = '2293df74-ff7f-43b9-80b1-61e89881e9c9';
  $display->content['new-2293df74-ff7f-43b9-80b1-61e89881e9c9'] = $pane;
  $display->panels['middle'][3] = 'new-2293df74-ff7f-43b9-80b1-61e89881e9c9';
  $pane = new stdClass();
  $pane->pid = 'new-a39ec35a-042d-46d7-b9c1-c89eabc389fd';
  $pane->panel = 'middle';
  $pane->type = 'custom';
  $pane->subtype = 'custom';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'admin_title' => 'Close JS',
    'title' => '',
    'body' => '<script>  
(function ($) {

$(".list-link").click(function name() {
    $("#dispatch-list-frame, #dash-slide-box-dispatch" , window.parent.document).animate({ "right": -330 },  "medium"); 
});

})(jQuery);
</script>',
    'format' => 'full_html',
    'substitute' => 1,
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 4;
  $pane->locks = array();
  $pane->uuid = 'a39ec35a-042d-46d7-b9c1-c89eabc389fd';
  $display->content['new-a39ec35a-042d-46d7-b9c1-c89eabc389fd'] = $pane;
  $display->panels['middle'][4] = 'new-a39ec35a-042d-46d7-b9c1-c89eabc389fd';
$display->hide_title = PANELS_TITLE_FIXED;
$display->title_pane = '0';
$handler->conf['display'] = $display;
