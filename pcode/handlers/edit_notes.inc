<?php

$handler = new stdClass();
$handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
$handler->api_version = 1;
$handler->name = 'edit_notes';
$handler->task = 'node_edit';
$handler->subtask = '';
$handler->handler = 'panel_context';
$handler->weight = 0;
$handler->conf = array(
  'title' => 'Note Edit',
  'no_blocks' => 0,
  'pipeline' => 'standard',
  'body_classes_to_remove' => '',
  'body_classes_to_add' => '',
  'css_id' => '',
  'css' => '',
  'contexts' => array(),
  'relationships' => array(),
  'access' => array(
    'plugins' => array(
      0 => array(
        'name' => 'role',
        'settings' => array(
          'rids' => array(
            0 => 225332102,
            1 => 172216448,
            2 => 185875538,
            3 => 40207975,
            4 => 203373462,
          ),
        ),
        'context' => 'logged-in-user',
        'not' => FALSE,
      ),
      1 => array(
        'name' => 'node_type',
        'settings' => array(
          'type' => array(
            'notes' => 'notes',
          ),
        ),
        'context' => 'argument_node_edit_1',
        'not' => FALSE,
      ),
    ),
    'logic' => 'and',
  ),
);
$display = new panels_display();
$display->layout = 'onecol';
$display->layout_settings = array();
$display->panel_settings = array(
  'style_settings' => array(
    'default' => NULL,
    'middle' => NULL,
  ),
);
$display->cache = array();
$display->title = '';
$display->uuid = '0c9a5df8-4625-43b6-85df-529ccbb32aa1';
$display->content = array();
$display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-9f9206ea-b1b5-42a2-a4a5-659e0368ccc9';
  $pane->panel = 'middle';
  $pane->type = 'block';
  $pane->subtype = 'block-5';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => 1,
    'override_title_text' => '',
    'override_title_heading' => 'h2',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '9f9206ea-b1b5-42a2-a4a5-659e0368ccc9';
  $display->content['new-9f9206ea-b1b5-42a2-a4a5-659e0368ccc9'] = $pane;
  $display->panels['middle'][0] = 'new-9f9206ea-b1b5-42a2-a4a5-659e0368ccc9';
  $pane = new stdClass();
  $pane->pid = 'new-fe774bb2-1c69-4e0b-a4b3-0779849e6c63';
  $pane->panel = 'middle';
  $pane->type = 'views';
  $pane->subtype = 'notes';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_pager_settings' => 0,
    'use_pager' => 1,
    'nodes_per_page' => '10',
    'pager_id' => '0',
    'offset' => '0',
    'more_link' => 0,
    'feed_icons' => 0,
    'panel_args' => 0,
    'link_to_view' => 0,
    'args' => '',
    'url' => '',
    'display' => 'block_1',
    'override_title' => 1,
    'override_title_text' => '',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array(
    'css_id' => 'notes-sidebar',
    'css_class' => 'second-nav',
  );
  $pane->extras = array();
  $pane->position = 1;
  $pane->locks = array();
  $pane->uuid = 'fe774bb2-1c69-4e0b-a4b3-0779849e6c63';
  $display->content['new-fe774bb2-1c69-4e0b-a4b3-0779849e6c63'] = $pane;
  $display->panels['middle'][1] = 'new-fe774bb2-1c69-4e0b-a4b3-0779849e6c63';
  $pane = new stdClass();
  $pane->pid = 'new-0e175235-eaa0-40fe-8f66-6e7d0cd004e3';
  $pane->panel = 'middle';
  $pane->type = 'node_form_title';
  $pane->subtype = 'node_form_title';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'context' => 'argument_node_edit_1',
    'override_title' => 0,
    'override_title_text' => '',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array(
    'css_id' => '',
    'css_class' => 'single-col-top',
  );
  $pane->extras = array();
  $pane->position = 2;
  $pane->locks = array();
  $pane->uuid = '0e175235-eaa0-40fe-8f66-6e7d0cd004e3';
  $display->content['new-0e175235-eaa0-40fe-8f66-6e7d0cd004e3'] = $pane;
  $display->panels['middle'][2] = 'new-0e175235-eaa0-40fe-8f66-6e7d0cd004e3';
  $pane = new stdClass();
  $pane->pid = 'new-afa05512-e994-4776-8630-f1d320ce3f02';
  $pane->panel = 'middle';
  $pane->type = 'entity_form_field';
  $pane->subtype = 'node:body';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'label' => '',
    'formatter' => '',
    'context' => 'argument_node_edit_1',
    'override_title' => 0,
    'override_title_text' => '',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array(
    'css_id' => '',
    'css_class' => 'single-col-mid',
  );
  $pane->extras = array();
  $pane->position = 3;
  $pane->locks = array();
  $pane->uuid = 'afa05512-e994-4776-8630-f1d320ce3f02';
  $display->content['new-afa05512-e994-4776-8630-f1d320ce3f02'] = $pane;
  $display->panels['middle'][3] = 'new-afa05512-e994-4776-8630-f1d320ce3f02';
  $pane = new stdClass();
  $pane->pid = 'new-4ab06c1e-11b4-4611-b880-103bd7c72ec1';
  $pane->panel = 'middle';
  $pane->type = 'node_form_buttons';
  $pane->subtype = 'node_form_buttons';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'context' => 'argument_node_edit_1',
    'override_title' => 0,
    'override_title_text' => '',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array(
    'css_id' => '',
    'css_class' => 'single-col-mid',
  );
  $pane->extras = array();
  $pane->position = 4;
  $pane->locks = array();
  $pane->uuid = '4ab06c1e-11b4-4611-b880-103bd7c72ec1';
  $display->content['new-4ab06c1e-11b4-4611-b880-103bd7c72ec1'] = $pane;
  $display->panels['middle'][4] = 'new-4ab06c1e-11b4-4611-b880-103bd7c72ec1';
$display->hide_title = PANELS_TITLE_FIXED;
$display->title_pane = '0';
$handler->conf['display'] = $display;
