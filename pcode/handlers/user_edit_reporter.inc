<?php

$handler = new stdClass();
$handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
$handler->api_version = 1;
$handler->name = 'user_edit_reporter';
$handler->task = 'user_edit';
$handler->subtask = '';
$handler->handler = 'panel_context';
$handler->weight = 0;
$handler->conf = array(
  'title' => 'reporter',
  'no_blocks' => 0,
  'pipeline' => 'standard',
  'body_classes_to_remove' => '',
  'body_classes_to_add' => '',
  'css_id' => '',
  'css' => '',
  'contexts' => array(),
  'relationships' => array(),
  'name' => 'reporter',
  'access' => array(
    'plugins' => array(
      0 => array(
        'name' => 'role',
        'settings' => array(
          'rids' => array(
            0 => 84425795,
          ),
        ),
        'context' => 'argument_user_edit_1',
        'not' => FALSE,
      ),
    ),
    'logic' => 'and',
  ),
);
$display = new panels_display();
$display->layout = 'onecol';
$display->layout_settings = array();
$display->panel_settings = array(
  'style_settings' => array(
    'default' => NULL,
    'middle' => NULL,
  ),
);
$display->cache = array();
$display->title = '';
$display->uuid = 'b5afa789-4f1a-49a1-9238-09488ebd4446';
$display->content = array();
$display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-0248e262-33bf-40b5-a9d3-85538aabb2fb';
  $pane->panel = 'middle';
  $pane->type = 'custom';
  $pane->subtype = 'custom';
  $pane->shown = TRUE;
  $pane->access = array(
    'plugins' => array(
      0 => array(
        'name' => 'role',
        'settings' => array(
          'rids' => array(
            0 => 215641394,
          ),
        ),
        'context' => 'argument_user_edit_1',
        'not' => FALSE,
      ),
    ),
  );
  $pane->configuration = array(
    'admin_title' => 'Pending Admin',
    'title' => '',
    'body' => '<ul class="member-mng-nav">
<li><a href="%user:edit-url">General</a></li>
</ul>',
    'format' => 'full_html',
    'substitute' => 1,
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '0248e262-33bf-40b5-a9d3-85538aabb2fb';
  $display->content['new-0248e262-33bf-40b5-a9d3-85538aabb2fb'] = $pane;
  $display->panels['middle'][0] = 'new-0248e262-33bf-40b5-a9d3-85538aabb2fb';
  $pane = new stdClass();
  $pane->pid = 'new-40f4dd86-0d58-42b8-86c2-f3940310fac6';
  $pane->panel = 'middle';
  $pane->type = 'custom';
  $pane->subtype = 'custom';
  $pane->shown = TRUE;
  $pane->access = array(
    'plugins' => array(
      0 => array(
        'name' => 'role',
        'settings' => array(
          'rids' => array(
            0 => 215641394,
          ),
        ),
        'context' => 'argument_user_edit_1',
        'not' => TRUE,
      ),
    ),
  );
  $pane->configuration = array(
    'admin_title' => 'Nav',
    'title' => '',
    'body' => '<ul class="member-mng-nav">
<li><a href="%user:edit-url">General</a></li>
<li><a href="?q=upgrade">Subscription</a></li>
</ul>',
    'format' => 'full_html',
    'substitute' => 1,
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 1;
  $pane->locks = array();
  $pane->uuid = '40f4dd86-0d58-42b8-86c2-f3940310fac6';
  $display->content['new-40f4dd86-0d58-42b8-86c2-f3940310fac6'] = $pane;
  $display->panels['middle'][1] = 'new-40f4dd86-0d58-42b8-86c2-f3940310fac6';
  $pane = new stdClass();
  $pane->pid = 'new-da899e9d-4c28-4072-b856-ab503cdb4b17';
  $pane->panel = 'middle';
  $pane->type = 'page_messages';
  $pane->subtype = 'page_messages';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array();
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array(
    'css_id' => '',
    'css_class' => 'profile-messages',
  );
  $pane->extras = array();
  $pane->position = 2;
  $pane->locks = array();
  $pane->uuid = 'da899e9d-4c28-4072-b856-ab503cdb4b17';
  $display->content['new-da899e9d-4c28-4072-b856-ab503cdb4b17'] = $pane;
  $display->panels['middle'][2] = 'new-da899e9d-4c28-4072-b856-ab503cdb4b17';
  $pane = new stdClass();
  $pane->pid = 'new-2cbca1ee-94f4-4420-8e50-af26de2d3a13';
  $pane->panel = 'middle';
  $pane->type = 'form';
  $pane->subtype = 'form';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'context' => 'argument_user_edit_1',
    'override_title' => 1,
    'override_title_text' => '',
    'override_title_heading' => 'h2',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 3;
  $pane->locks = array();
  $pane->uuid = '2cbca1ee-94f4-4420-8e50-af26de2d3a13';
  $display->content['new-2cbca1ee-94f4-4420-8e50-af26de2d3a13'] = $pane;
  $display->panels['middle'][3] = 'new-2cbca1ee-94f4-4420-8e50-af26de2d3a13';
$display->hide_title = PANELS_TITLE_FIXED;
$display->title_pane = 'new-40f4dd86-0d58-42b8-86c2-f3940310fac6';
$handler->conf['display'] = $display;
