<?php

$handler = new stdClass();
$handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
$handler->api_version = 1;
$handler->name = 'bulletin_add_node';
$handler->task = 'pm_existing_pages';
$handler->subtask = 'bulletin_add';
$handler->handler = 'panel_context';
$handler->weight = 0;
$handler->conf = array(
  'title' => 'citizen-add',
  'no_blocks' => 0,
  'pipeline' => 'standard',
  'body_classes_to_remove' => '',
  'body_classes_to_add' => '',
  'css_id' => '',
  'css' => '',
  'contexts' => array(),
  'relationships' => array(),
);
$display = new panels_display();
$display->layout = 'onecol';
$display->layout_settings = array();
$display->panel_settings = array(
  'style_settings' => array(
    'default' => NULL,
    'middle' => NULL,
  ),
);
$display->cache = array();
$display->title = '';
$display->uuid = '9443ef4d-e239-42fb-85f2-c97a7aba28a4';
$display->content = array();
$display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-99f9a2ff-0a7c-4d0a-becd-1d1bc5017d34';
  $pane->panel = 'middle';
  $pane->type = 'custom';
  $pane->subtype = 'custom';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'admin_title' => 'Top Nav',
    'title' => '',
    'body' => '<div class="navigation-bar nodebar-bg">	
	<div class="main-bar-items">
		<div class="nav-text">Post a Report</div>
		<div class="right-blox">
			<ul>
				<li class="nav-icon"><div id="node-close" class="x-icon"></div></li>
			</ul>
		</div>
	</div>
</div>',
    'format' => 'full_html',
    'substitute' => 1,
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '99f9a2ff-0a7c-4d0a-becd-1d1bc5017d34';
  $display->content['new-99f9a2ff-0a7c-4d0a-becd-1d1bc5017d34'] = $pane;
  $display->panels['middle'][0] = 'new-99f9a2ff-0a7c-4d0a-becd-1d1bc5017d34';
  $pane = new stdClass();
  $pane->pid = 'new-8664957c-8731-47f9-b2e4-fee11164ba1c';
  $pane->panel = 'middle';
  $pane->type = 'pm_existing_pages';
  $pane->subtype = 'pm_existing_pages';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    0 => 'task_id',
    'task_id' => 'bulletin_add',
    'override_title' => 1,
    'override_title_text' => '',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array(
    'css_id' => '',
    'css_class' => 'main-body',
  );
  $pane->extras = array();
  $pane->position = 1;
  $pane->locks = array();
  $pane->uuid = '8664957c-8731-47f9-b2e4-fee11164ba1c';
  $display->content['new-8664957c-8731-47f9-b2e4-fee11164ba1c'] = $pane;
  $display->panels['middle'][1] = 'new-8664957c-8731-47f9-b2e4-fee11164ba1c';
  $pane = new stdClass();
  $pane->pid = 'new-2774e144-601e-4753-a451-0435f4539276';
  $pane->panel = 'middle';
  $pane->type = 'custom';
  $pane->subtype = 'custom';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'admin_title' => 'Location Get',
    'title' => '',
    'body' => '<script>
(function ($) {
$(document).ready(function () {
	     $(\'#edit-field-tip-location-und-0-geom-lat\').val(window.parent.$(\'#lat\').val());
      	     $(\'#edit-field-tip-location-und-0-geom-lon\').val(window.parent.$(\'#long\').val());      
});
})(jQuery);
</script>',
    'format' => 'full_html',
    'substitute' => 1,
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 2;
  $pane->locks = array();
  $pane->uuid = '2774e144-601e-4753-a451-0435f4539276';
  $display->content['new-2774e144-601e-4753-a451-0435f4539276'] = $pane;
  $display->panels['middle'][2] = 'new-2774e144-601e-4753-a451-0435f4539276';
$display->hide_title = PANELS_TITLE_NONE;
$display->title_pane = '0';
$handler->conf['display'] = $display;
